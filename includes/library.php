<?php
/*
Description: Library page
*/
function edent_library_shortcode() {

	$output = "";

	$args = array(
		'tag'            => "book-review",
		'posts_per_page' => -1
	);
	$query = new WP_Query( $args );

	$year = "";

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$post_id   = get_the_ID();
			$link      = get_permalink();
			$image     = get_the_post_thumbnail_url();
			$post_year = get_the_date('Y');

			if ( $post_year != $year ) {
				if ( $year != "" ) {
					$output .= "</ul>";
				}
				$output .= "<h2>{$post_year}</h2>\n";
				$output .= "<ul class='related-posts'>";
				$year = $post_year;
			}
			$review_data = get_post_meta( $post_id, "_edent_review_meta_key", true );
			if ( isset( $review_data["review"] ) )
			{
				if ($review_data["review"] == "book")
				{
					$title  = $review_data["book"]["title"];
					$author = $review_data["book"]["author"];
					$rating = $review_data["book"]["rating"];
					$output .= "<li class='related-post' style='min-width: 9em !important;'>";
					
						$output .= "<a href='{$link}'>";

							$output .= "<img src='{$image}' width='100' loading='lazy' alt='{$title} by {$author}' style='height: auto !important;'>";
							
							$output .= "{$title} by {$author}";
						
						$output .= "</a>";

						$output .= "<span class='edent-rating-stars' role='img' aria-label='Rating: {$rating} out of 5 stars'><span aria-hidden=true style='font-size: .75em !important;'>";
						$full = floor($rating);
						$half = 0;
						if ($rating - $full == 0.5) {
							$half = 1;
						}

						$empty = 5 - $half - $full;

						for ($i=0; $i < $full ; $i++) {
							$output .= "★";
						}
						if ($half == 1) {
							$output .= "⯪";
						}
						for ($i=0; $i < $empty ; $i++) {
							$output .= "☆";
						}
						$output .= "</span></span>";
					
					$output .= "</li>";
				}
			} else {
				$output .= "<li class='related-post' style='min-width: 9em !important;'>";
					$output .= "<a href='{$link}'>";

						$output .= "<img src='{$image}' width='100' loading='lazy' alt='Book Cover' style='height: auto !important;'>";
				
						$output .= get_the_title();
				
					$output .= "</a>";
				
				$output .= "</li>";
			}
		}
		$output .= '</ul>';
	} else {
		// no posts found
	}
	wp_reset_query();
	return $output;
}

//	Set up the shortcode
function edent_library_shortcode_init() {
	add_shortcode( 'edent_library', 'edent_library_shortcode' );
}
add_action( 'init', 'edent_library_shortcode_init' );
