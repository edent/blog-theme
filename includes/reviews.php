<?php
/* Reviews */
//	https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/#adding-meta-boxes

//	Display the Review Metadata box
function edent_add_review_custom_box() {	
	$screens = ['post'];
	foreach ( $screens as $screen ) {
	  add_meta_box(
			'edent_review_box_id',  // Unique ID
			'Review Metadata',      // Box title
			'edent_review_box_html',// Content callback, must be of type callable
			$screen                 // Post type
	   );
	}
}
add_action('add_meta_boxes', 'edent_add_review_custom_box');

//	HTML for the box
function edent_review_box_html( $post ) {
	//	Get any existing review metadata
	$review_data = get_post_meta( get_the_ID(), "_edent_review_meta_key", true );

	//	Assume no review to start with
	$checked_empty = "checked";
	$open = "";

	//	If review available
	$review_type = null;
	if (!empty($review_data)) {
		$checked_empty = "";
		$review_type = $review_data["review"];
	}
	echo "<input type=radio id=edent_no_review name=edent_review[review] value='' {$checked_empty}> <label for=edent_no_review>No Review:</label>";

	//	Book review
	$checked_book    = "";
	$open_book       = "";
	$rating_book     = "";
	$book_isbn       = ""; 
	$book_title      = "";
	$book_url        = "";
	$book_date       = "";
	$book_author     = "";
	$book_author_url = "";
	$book_amazon_url = "";
	$book_hive_url   = "";

	if ($review_type == "book") {
		$checked_book    = "checked";
		$open_book       = "open";
		$rating_book     = esc_html($review_data["book"]["rating"]);
		$book_isbn       = esc_html($review_data['book']["isbn"]);
		$book_title      = esc_html($review_data['book']["title"]);
		$book_url        = esc_html($review_data["book"]["url"]);
		$book_date       = esc_html($review_data['book']["date"]);
		$book_author     = esc_html($review_data['book']["author"]);
		$book_author_url = esc_html($review_data['book']["author_url"]);
		$book_amazon_url = esc_html($review_data['book']["amazon_url"]);
		$book_hive_url   = esc_html($review_data['book']["hive_url"]);
	}
	echo <<<EOT
	<details $open_book>
		<summary>Book Review</summary>
		<table>
			<tr>
				<td style="text-align: right;"><label for=edent_book_review>Embed Book Review:</label></td>
				<td><input type=radio id=edent_book_review name=edent_review[review] value=book $checked_book>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_book_rating>Rating:</label></td>
				<td><input type=range id=edent_book_rating name=edent_review[book][rating] min=0 max=5 step=0.5 value="$rating_book">
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_isbn >ISBN:</label></td>
				<td><input name=edent_review[book][isbn]  id=edent_isbn type=text value="$book_isbn" autocomplete=off><button type="button" id="fetchButton">Fetch Book Info</button></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_book_title>Book Title:</label></td>
				<td><input name=edent_review[book][title] id=edent_book_title type=text value="$book_title" autocomplete=off></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_book_url>Publisher URl:</label></td>
				<td><input name=edent_review[book][url] id=edent_book_url type=url value="$book_url" autocomplete=off></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_book_date>Published Date:</label></td>
				<td><input name=edent_review[book][date] id=edent_book_date type=text pattern='\d{4}(-\d{2})?(-\d{2})?' value="$book_date" autocomplete=off></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_author>Author:</label></td>
				<td><input name=edent_review[book][author] id=edent_author type=text value="$book_author" autocomplete=off></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_author_url>Author URl:</label></td>
				<td><input name=edent_review[book][author_url] id=edent_author_url type=url value="$book_author_url" autocomplete=off></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_amazon_url>Amazon URl:</label></td>
				<td><input name=edent_review[book][amazon_url] id=edent_amazon_url type=url value="$book_amazon_url" autocomplete=off></td>
			</tr>
			<tr>
				<td style="text-align: right;"><label for=edent_hive_url>Hive URl:</label></td>
				<td><input name=edent_review[book][hive_url] id=edent_hive_url type=url value="$book_hive_url" autocomplete=off></td>
			</tr>
		</table>
	</details>
	EOT;

	echo <<<'EOJS'

	<script>
		document.getElementById('fetchButton').addEventListener('click', function () {
		//	Get the value of the ISBN field
		const isbn = document.getElementById('edent_isbn').value;

		//	Ensure the ISBN field is not empty
		if (!isbn) {
			alert('Please enter an ISBN.');
			return;
		}

		//	Construct the Google Books API URl
		const apiUrl = `https://www.googleapis.com/books/v1/volumes?q=isbn:${isbn}`;

		//	Make the API request to Google Books
		fetch(apiUrl)
			.then(response => {
				if (!response.ok) {
					throw new Error('Network response was not ok');
				}
				return response.json();
			})
			.then(data => {
				//	Check if there are items in the response
				if (data.items && data.items.length > 0) {
					const bookInfo = data.items[0].volumeInfo;

					//	Fill in the Author field
					bookAuthor = bookInfo.authors ? bookInfo.authors[0] : 'Unknown Author';
					document.getElementById('edent_author').value = bookAuthor;

					//	Fill in the Title field with title and subtitle (if available)
					const title = bookInfo.title || 'Unknown Title';
					const subtitle = bookInfo.subtitle ? ` - ${bookInfo.subtitle}` : '';
					bookTitle = title + subtitle;
					document.getElementById('edent_book_title').value = bookTitle;
					
					//	Fill in the Date field
					document.getElementById('edent_book_date').value = bookInfo.publishedDate ? bookInfo.publishedDate : '0';

					document.getElementById('title').value = "Book Review: " + bookTitle + " by " + bookAuthor;

				} else {
					alert('No books found for this ISBN.');
				}
			})
			.catch(error => {
				console.error('There was a problem with the fetch operation:', error);
				alert('Failed to fetch book information.');
			});
		});
	</script>
	EOJS;

	//	Movie
	$checked_movie = "";
	$open_movie    = "";
	$movie_rating  = "";
	$movie_eidr    = "";
	$movie_title   = "";
	$movie_url     = "";
	$movie_date    = "";
	$movie_dir     = "";

	if ($review_type  == "movie") {
		$checked_movie = "checked";
		$open_movie = "open";
		$movie_rating = esc_html($review_data['movie']["rating"]);
		$movie_eidr   = esc_html($review_data['movie']["eidr"]);
		$movie_title  = esc_html($review_data['movie']["title"]);
		$movie_url    = esc_html($review_data['movie']["url"]);
		$movie_date   = esc_html($review_data['movie']["date"]);
		$movie_dir    = esc_html($review_data['movie']["director"]);
	} 
	echo <<<EOT
	<details $open_movie>
		<summary>Movie Review</summary>
		<table>
			<tr>
				<td><label for=edent_movie_review>Embed Movie Review:</label></td>
				<td><input type=radio id=edent_movie_review name=edent_review[review] value=movie $checked_movie></td>
			</tr>
			<tr>
				<td><label for=edent_movie_rating>Rating:</label></td>
				<td><input type=range id=edent_movie_rating name=edent_review[movie][rating] min=0 max=5 step=0.5 value="$movie_rating"></td>
			</tr>
			<tr>
				<td><label for=edent_eidr>EIDR:</label></td>
				<td><input name=edent_review[movie][eidr] id=edent_eidr type=text value="$movie_eidr" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_movie_title>Movie Title:</label></td>
				<td><input name=edent_review[movie][title] id=edent_movie_title type=text value="$movie_title" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_movie_url>Movie URl:</label></td>
				<td><input name=edent_review[movie][url] id=edent_movie_url type=url value="$movie_url" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_movie_date>Release Date:</label></td>
				<td><input name=edent_review[movie][date] id=edent_movie_date type=text pattern='\d{4}(-\d{2})?(-\d{2})?' value="$movie_date"' autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_director>Director:</label></td>
				<td><input name=edent_review[movie][director] id=edent_director type=text value="$movie_dir" autocomplete=off></td>
			</tr>
		</table>
	</details>
	EOT;
	
	//	Game
	$checked_game = "";
	$open_game    = "";
	$game_rating  = "";
	$game_os      = "";
	$game_title   = "";
	$game_url     = "";

	if ($review_type == "game") {
		$checked_game = "checked";
		$open_game    = "open";
		$game_rating  = esc_html($review_data['game']["rating"]);
		$game_os      = esc_html($review_data['game']["os"]);
		$game_title   = esc_html($review_data['game']["title"]);
		$game_url     = esc_html($review_data['game']["url"]);
	} 
	echo <<<EOT
	<details $open_game>
		<summary>Game Review</summary>
		<table>
			<tr>
				<td><label for=edent_game_review>Embed Game Review:</label></td>
				<td><input type=radio id=edent_game_review name=edent_review[review] value=game $checked_game></td>
			</tr>
			<tr>
				<td><label for=edent_game_rating>Rating:</label></td>
				<td><input type=range id=edent_game_rating name=edent_review[game][rating] min=0 max=5 step=0.5 value="$game_rating"></td>
			</tr>
			<tr>
				<td><label for=edent_os>Operating System:</label></td>
				<td><input name=edent_review[game][os] id=edent_os type=text value="$game_os" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_game_title>Game Title:</label></td>
				<td><input name=edent_review[game][title] id=edent_game_title type=text value="$game_title" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_game_url>Game URl:</label></td>
				<td><input name=edent_review[game][url] id=edent_game_url type=url value="$game_url" autocomplete=off></td>
			</tr>
		</table>
	</details>
	EOT;

	//	Product
	$checked_product = "";
	$open_product    = "";
	$product_rating  = "";
	$product_brand   = "";
	$product_title   = "";
	$product_info    = "";
	$product_url     = "";

	if ($review_type == "product") {
		$checked_product = "checked";
		$open_product = "open";
		$product_rating = esc_html($review_data['product']["rating"]);
		$product_brand  = esc_html($review_data['product']["brand"]);
		$product_title  = esc_html($review_data['product']["title"]);
		$product_info   = esc_html($review_data['product']["description"]);
		$product_url    = esc_html($review_data['product']["url"]);
	} 
	echo <<<EOT
	<details $open_product>
		<summary>Product Review</summary>
		<table>
			<tr>
				<td><label for=edent_product_review>Embed Product Review:</label></td>
				<td><input type=radio id=edent_product_review name=edent_review[review] value=product $checked_product></td>
			</tr>
			<tr>
				<td><label for=edent_product_rating>Rating:</label></td>
				<td><input type=range id=edent_product_rating name=edent_review[product][rating] min=0 max=5 step=0.5 value="$product_rating"></td>
			</tr>
			<tr>
				<td><label for=edent_brand>Brand:</label></td>
				<td><input name=edent_review[product][brand] id=edent_brand type=text value="$product_brand" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_product_title>Product Title:</label></td>
				<td><input name=edent_review[product][title] id=edent_product_title type=text value="$product_title" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_product_description>Product description:</label></td>
				<td><input name=edent_review[product][description] id=edent_product_description type=text value="$product_info" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_product_url>Product URl:</label></td>
				<td><input name=edent_review[product][url] id=edent_product_url type=url value="$product_url" autocomplete=off></td>
			</tr>
		</table>
	</details>
	EOT;

	//	restaurant
	$checked_restaurant  = "";
	$open_restaurant     = "";
	$restaurant_rating   = "";
	$restaurant_name     = "";
	$restaurant_address  = "";
	$restaurant_menu     = "";
	$restaurant_url      = "";
	$restaurant_cuisine  = "";
	$restaurant_happycow = "";

	if ($review_type == "restaurant") {
		$checked_restaurant = "checked";
		$open_restaurant = "open";
		$restaurant_rating   = esc_html($review_data['restaurant']["rating"]);
		$restaurant_name     = esc_html($review_data['restaurant']["name"]);
		$restaurant_address  = esc_html($review_data['restaurant']["address"]);
		$restaurant_menu     = esc_html($review_data['restaurant']["menu"]);
		$restaurant_url      = esc_html($review_data['restaurant']["url"]);
		$restaurant_cuisine  = esc_html($review_data['restaurant']["cuisine"]);
		$restaurant_happycow = esc_html($review_data['restaurant']["happycow"]);
	} 
	echo <<<EOT
	<details $open_restaurant>
		<summary>Restaurant Review</summary>
		<table>
			<tr>
				<td><label for=edent_restaurant_review>Embed Restaurant Review:</label></td>
				<td><input type=radio id=edent_restaurant_review name=edent_review[review] value=restaurant $checked_restaurant></td>
			</tr>
			<tr>
				<td><label for=edent_restaurant_rating>Rating:</label></td>
				<td><input type=range id=edent_restaurant_rating name=edent_review[restaurant][rating] min=0 max=5 step=0.5 value="$restaurant_rating"></td>
			</tr>
			<tr>
				<td><label for=edent_restaurant_name>Name:</label></td>
				<td><input name=edent_review[restaurant][name] id=edent_restaurant_name type=text value="$restaurant_name" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_restaurant_address>Address:</label></td>
				<td><input name=edent_review[restaurant][address] id=edent_restaurant_address type=text value="$restaurant_address" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_restaurant_url>Restaurant URl:</label></td>
				<td><input name=edent_review[restaurant][url] id=edent_restaurant_url type=url value="$restaurant_url" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_restaurant_menu>Restaurant Menu URl:</label></td>
				<td><input name=edent_review[restaurant][menu] id=edent_restaurant_menu type=url value="$restaurant_menu" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_restaurant_menu>HappyCow URl:</label></td>
				<td><input name=edent_review[restaurant][happycow] id=edent_restaurant_happycow type=url value="$restaurant_happycow" autocomplete=off></td>
			</tr>	
			<tr>
				<td><label for=edent_restaurant_cuisine>Restaurant Cuisine:</label></td>
				<td><input name=edent_review[restaurant][cuisine] id=edent_restaurant_cuisine type=text value="$restaurant_cuisine" autocomplete=off></td>
			</tr>
		</table>
	</details>
	EOT;

	//	Event
	$checked_event   = "";
	$open_event      = "";
	$event_rating    = "";
	$event_location  = "";
	$event_director  = "";
	$event_performer = "";
	$event_title     = "";
	$event_url       = "";

	if ($review_type == "event") {
		$checked_event = "checked";
		$open_event    = "open";
		$event_rating    = esc_html($review_data['event']["rating"]);
		$event_location  = esc_html($review_data['event']["location"]);
		$event_director  = esc_html($review_data['event']["director"]);
		$event_performer = esc_html($review_data['event']["performer"]);
		$event_title     = esc_html($review_data['event']["title"]);
		$event_url       = esc_html($review_data['event']["url"]);
	} 
	echo <<<EOT
	<details $open_event>
		<summary>Event Review</summary>
		<table>
			<tr>
				<td><label for=edent_event_review>Embed Event Review:</label></td>
				<td><input type=radio id=edent_event_review name=edent_review[review] value=event $checked_event></td>
			</tr>
			<tr>
				<td><label for=edent_event_rating>Rating:</label></td>
				<td><input type=range id=edent_event_rating name=edent_review[event][rating] min=0 max=5 step=0.5 value="$event_rating"></td>
			</tr>
			<tr>
				<td><label for=edent_location>Location:</label></td>
				<td><input name=edent_review[event][location] id=edent_location type=text value="$event_location" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_event_director>Event Director:</label></td>
				<td><input name=edent_review[event][director] id=edent_event_director type=text value="$event_director" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_event_performer>Event Performer:</label></td>
				<td><input name=edent_review[event][performer] id=edent_event_performer type=text value="$event_performer" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_event_title>Event Title:</label></td>
				<td><input name=edent_review[event][title] id=edent_event_title type=text value="$event_title" autocomplete=off></td>
			</tr>
			<tr>
				<td><label for=edent_event_url>Event URl:</label></td>
				<td><input name=edent_review[event][url] id=edent_event_url type=url value="$event_url" autocomplete=off></td>
			</tr>
		</table>
	</details>
	EOT;

	//	Debug
	echo "<details><summary>Debug</summary><pre>";
	var_export($review_data);
	echo "</pre></details>";
}

//	Save the review
function edent_review_save_postdata($post_id) {
	if ( array_key_exists( 'edent_review', $_POST ) ) {
		$review_type = $_POST['edent_review']["review"];

		if ($review_type != "") {
			$review_data = $_POST['edent_review'][$review_type];
			//	Trim all text
			array_walk_recursive($review_data, function(&$v) { $v = trim($v); });

			update_post_meta(
				$post_id,
				'_edent_review_meta_key',
				array('review' => $review_type, $review_type => $review_data)
				// $_POST['edent_review']
			);
		} else {
			delete_post_meta(
				$post_id,
				'_edent_review_meta_key'
			);
		}
	}
}
add_action('save_post', 'edent_review_save_postdata');

function edent_review_display($post_id) {
	// https://developer.wordpress.org/reference/functions/the_meta/
	$review_data = get_post_meta( $post_id, "_edent_review_meta_key", true );
	// if ($review_data["review"] != "")
	if ( isset( $review_data["review"] ) )
	{
		$url_html = "";

		if ($review_data["review"] == "book")
		{
			$review_array = array (
				'@type' => 'Book',
				'name'          => $review_data["book"]["title"],
				'isbn'          => $review_data["book"]["isbn"],
				'sameAs'        => $review_data["book"]["url"],
				'datePublished' => $review_data["book"]["date"],
				'author' =>
				array (
					'@type'  => 'Person',
					'name'	=> $review_data["book"]["author"],
					'sameAs' => $review_data["book"]["author_url"]
				),
			);

			//	Format ISBN
			if ( isset($review_data["book"]["isbn"] ) && $review_data["book"]["isbn"] != "" ) {
				$isbn = $review_data["book"]["isbn"];
				//	Remove any formatting characters
				//	Might include X as a check digit https://isbn-information.com/check-digit-for-the-13-digit-isbn.html
				$isbn = preg_replace("/[^0-9X]/i", "", $isbn );
			} else {
				$isbn = false;
			}

			//	Format Search String
			$search_text = urlencode( $review_data["book"]["title"] . " " . $review_data["book"]["author"] );

			//	Amazon (link or search)
			if ( isset($review_data["book"]["amazon_url"]) && $review_data["book"]["amazon_url"] != "" ) {
				$url_html .= "<li><a href='".esc_url($review_data["book"]["amazon_url"])."'>Read on Amazon Kindle</a></li>";
			} else if ( $isbn ) {
				$url_html .= "<li><a href='https://www.amazon.co.uk/s?k=isbn%3A{$isbn}&linkCode=ll2&tag=shksprblog-21'>Read on Amazon Kindle</a></li>";
			}

			//	Kobo ISBN
			if ( $isbn ) {
				$url_html .= "<li><a href='https://click.linksynergy.com/deeplink?id=gvrGVKHEcVM&mid=37217&murl=https%3A%2F%2Fwww.kobo.com%2Fsearch%3Fquery%3D{$isbn}'>Audiobook and ePub from Kobo</a></li>";
			}

			//	Hive (link or search)
			if ( isset( $review_data["book"]["hive_url"]) && $review_data["book"]["hive_url"] != "" )  {
				// $url_html .= "<li><a href='https://track.webgains.com/click.html?wgcampaignid=1455705&wgprogramid=10671&clickref=shkspr&wgtarget=".esc_url($review_data["book"]["hive_url"])."'>Get the paper book from Hive</a></li>";
				$url_html .= "<li><a href='" . $review_data["book"]["hive_url"] . "'>Paper book from Hive</a></li>";
			} else if ( isset( $review_data["book"]["title"] ) && $review_data["book"]["title"] != "" )  {
				$url_html .= "<li><a href='https://www.hive.co.uk/Search/Books?fq=-01120&Keyword={$search_text}'>Paper book from Hive</a></li>";
			}

			//	Audible (search)
			if ( isset( $review_data["book"]["title"] ) && $review_data["book"]["title"] != "" )  {
				$url_html .= "<li><a href='https://www.amazon.co.uk/s?k={$search_text}&i=audible&linkCode=ll2&tag=shksprblog-21'>Listen on Audible</a></li>";
			}
			
			//	Author
			if ( isset($review_data["book"]["author_url"]) && $review_data["book"]["author_url"] != "" ) {
				$url_html .= "<li><a href='".esc_url($review_data["book"]["author_url"])."'>Author's homepage</a></li>";
			}

			//	Publisher
			if ( isset($review_data["book"]["url"]) && $review_data["book"]["url"] != "" ) {
				$url_html .= "<li><a href='".esc_url($review_data["book"]["url"])."'>Publisher's details</a></li>";
			}
			
			//	Library ISBN
			if ( $isbn ) {
				
				$url_html .= "<li><a href='https://worldcat.org/search?q=bn%3A{$isbn}'>Borrow from your local library</a></li>";

				$url_html .= "<li><a href='https://openlibrary.org/isbn/{$isbn}'>ISBN: {$isbn}</a></li>";
			}

			$review_rating =  $review_data["book"]["rating"];
		} elseif($review_data["review"] == "movie") {
			$review_array = array (
				'@type'         => 'Movie',
				'name'          => $review_data["movie"]["title"],
				'titleEIDR'     => $review_data["movie"]["eidr"],
				'sameAs'	       => $review_data["movie"]["url"],
				'datePublished' => $review_data["movie"]["date"],
				'image'         => get_the_post_thumbnail_url(),
				'director'      =>
					array (
						'@type' => 'Person',
						'name'  => $review_data["movie"]["director"]
					),
			);
			$review_rating =  $review_data["movie"]["rating"];
			if ($review_data["movie"]["url"] != "") {
				$url_html .= "<li><a href='".esc_url($review_data["movie"]["url"])."'>Movie website</a></li>";
			}
		} elseif($review_data["review"] == "game") {
			$review_array = array (
				'@type'               => 'VideoGame',
				'name'                => $review_data["game"]["title"],
				'sameAs'              => $review_data["game"]["url"],
				'operatingSystem'     => $review_data["game"]["os"],
				'gamePlatform'        => $review_data["game"]["os"],
				'applicationCategory' => 'Game',
				'image'               => get_the_post_thumbnail_url()
			);
			$review_rating =  $review_data["game"]["rating"];
			if ($review_data["game"]["url"] != "") {
				$url_html .= "<li><a href='".esc_url($review_data["game"]["url"])."'>Game website</a></li>";
			}
		} elseif($review_data["review"] == "product") {
			$review_array = array (
				'@type' => 'Product',
				'name'            => $review_data["product"]["title"],
				'brand'           => $review_data["product"]["brand"],
				'sameAs'          => $review_data["product"]["url"],
				'description'     => $review_data["product"]["description"],
				'image'           => get_the_post_thumbnail_url(),
				'aggregateRating' =>
					array (
						'@type' => 'AggregateRating',
						'ratingValue' => $review_data["product"]["rating"],
						'reviewCount' => "1",
						'worstRating' => 0,
						'bestRating'  => 5
					),
			);
			$review_rating =  $review_data["product"]["rating"];
			if ($review_data["product"]["url"] != "") {
				$url_html .= "<li><a href='".esc_url($review_data["product"]["url"])."'>Product website</a></li>";
			}
		} elseif($review_data["review"] == "event") {
			$review_array = array (
				'@type' => 'Event',
				'workPerformed'   => $review_data["event"]["title"],
				'name'            => $review_data["event"]["title"],
				'location'        => $review_data["event"]["location"],
				'sameAs'          => $review_data["event"]["url"],
				'director'        => $review_data["event"]["director"],
				'performer'       => $review_data["event"]["performer"],
				'image'           => get_the_post_thumbnail_url(),
				'aggregateRating' =>
					array (
						'@type' => 'AggregateRating',
						'ratingValue' => $review_data["event"]["rating"],
						'reviewCount' => "1",
						'worstRating' => 0,
						'bestRating'  => 5
					),
			);
			$review_rating =  $review_data["event"]["rating"];
			if ($review_data["event"]["url"] != "") {
				$url_html .= "<li><a href='".esc_url($review_data["event"]["url"])."'>Event website</a></li>";
			}
		}  elseif($review_data["review"] == "restaurant") {
			$review_array = array (
				'@type' => 'Restaurant',
				'name'            => $review_data["restaurant"]["name"],
				'address'         => $review_data["restaurant"]["address"],
				'sameAs'          => $review_data["restaurant"]["url"],
				'hasMenu'         => $review_data["restaurant"]["menu"],
				'servesCuisine'   => $review_data["restaurant"]["cuisine"],
				'image'           => get_the_post_thumbnail_url(),
				'aggregateRating' =>
					array (
						'@type' => 'AggregateRating',
						'ratingValue' => $review_data["restaurant"]["rating"],
						'reviewCount' => "1",
						'worstRating' => 0,
						'bestRating'  => 5
					),
			);
			$review_rating =  $review_data["restaurant"]["rating"];
			if ($review_data["restaurant"]["url"] != "") {
				$url_html .= "<li>🍽️ <a href='".esc_url($review_data["restaurant"]["url"])."'>Restaurant website</a></li>";
			}

			if ($review_data["restaurant"]["menu"] != "") {
				$url_html .= "<li>😋 <a href='".esc_url($review_data["restaurant"]["menu"])."'>Restaurant menu</a></li>";
			}

			if ($review_data["restaurant"]["happycow"] != "") {
				$url_html .= "<li>🐮 <a href='".esc_url($review_data["restaurant"]["happycow"])."'>HappyCow Reviews</a></li>";
			}
		}

		$blog_author_data = get_the_author_meta();

		$schema_review = array (
			'@context' => 'https://schema.org',
			'@type'	  => 'Review',
			'author'   =>
				array (
					'@type' => 'Person',
					'name'  => get_the_author_meta("user_firstname") . " " . get_the_author_meta("user_lastname"),
					'sameAs' =>
					array (
						0 => get_the_author_meta("user_url"),
					),
				),
			'url'           => get_permalink(),
			'datePublished' => get_the_date('c'),
			'publisher'     =>
				array (
					'@type'  => 'Organization',
					'name'	=> get_bloginfo("name"),
					'sameAs' => get_bloginfo("url"),
				),
			'description'  => mb_substr(get_the_excerpt(), 0, 198),
			'inLanguage'   => get_bloginfo("language"),
			'itemReviewed' => $review_array,
			'reviewRating' =>
				array (
					'@type'  => 'Rating',
					'worstRating' => 0,
					'bestRating'  => 5,
					'ratingValue' => $review_rating,
				),
			'thumbnailUrl' => get_the_post_thumbnail_url(),
		);
		echo '<script type="application/ld+json">' . json_encode($schema_review) . '</script>';

		//	Display Star Rating block
		echo "<table class='edent-rating-table'><thead><tr><th>Verdict</th></tr></thead>";
		echo "<tbody><tr><td><span class='edent-rating-stars' role='img' aria-label='Rating: {$review_rating} out of 5 stars'><span aria-hidden='true'>";
		$full = floor($review_rating);
		$half = 0;
		if ($review_rating - $full == 0.5) {
			$half = 1;
		}

		$empty = 5 - $half - $full;

		for ($i=0; $i < $full ; $i++) {
			echo "★";
		}
		if ($half == 1) {
			echo "⯪";
		}
		for ($i=0; $i < $empty ; $i++) {
			echo "☆";
		}
		echo "</span></span></td></tr></tbody></table>";

		//	Display websites
		echo "<ul>{$url_html}</ul>";

		//	Display Wishlist for Book Reviews only
		if ($review_data["review"] == "book") {
			echo "📚 Enjoyed this review? <a href=\"https://amzn.to/2SryYWf\">Buy me a book from my wishlist</a>.";
		}
	}
}

function edent_review_display_stars($post_id) {
	// https://developer.wordpress.org/reference/functions/the_meta/
	$review_data = get_post_meta($post_id, "_edent_review_meta_key", true);
	if ( isset( $review_data["review"] ) )
	{
		$url_html = "";

		if ($review_data["review"] == "book") {
			$review_rating =  $review_data["book"]["rating"];
		} elseif($review_data["review"] == "movie") {
			$review_rating =  $review_data["movie"]["rating"];
		} elseif($review_data["review"] == "game") {
			$review_rating =  $review_data["game"]["rating"];
		} elseif($review_data["review"] == "product") {
			$review_rating =  $review_data["product"]["rating"];
		} elseif($review_data["review"] == "event") {
			$review_rating =  $review_data["event"]["rating"];
		} elseif($review_data["review"] == "restaurant") {
			$review_rating =  $review_data["restaurant"]["rating"];
		}

		//	Display Star Rating block
		echo "<span class='edent-rating-stars' role='img' aria-label='Rating: {$review_rating} out of 5 stars'><span aria-hidden='true'>";
		$full = floor($review_rating);
		$half = 0;
		if ($review_rating - $full == 0.5) {
			$half = 1;
		}

		$empty = 5 - $half - $full;

		for ($i=0; $i < $full ; $i++) {
			echo "★";
		}
		if ($half == 1) {
			echo "⯪";
		}
		for ($i=0; $i < $empty ; $i++) {
			echo "☆";
		}
		echo "</span></span>";

	}
}

function edent_review_get_stars( $post_id ) {
	// https://developer.wordpress.org/reference/functions/the_meta/
	$review_data = get_post_meta( $post_id, "_edent_review_meta_key", true );
	$stars = null;
	if (isset($review_data["review"]))
	{
		if ($review_data["review"] == "book") {
			$review_rating =  $review_data["book"]["rating"];
		} elseif($review_data["review"] == "movie") {
			$review_rating =  $review_data["movie"]["rating"];
		} elseif($review_data["review"] == "game") {
			$review_rating =  $review_data["game"]["rating"];
		} elseif($review_data["review"] == "product") {
			$review_rating =  $review_data["product"]["rating"];
		} elseif($review_data["review"] == "event") {
			$review_rating =  $review_data["event"]["rating"];
		} elseif($review_data["review"] == "restaurant") {
			$review_rating =  $review_data["restaurant"]["rating"];
		}

		$full = floor($review_rating);
		$half = 0;
		if ($review_rating - $full == 0.5) {
			$half = 1;
		}

		$empty = 5 - $half - $full;

		for ($i=0; $i < $full ; $i++) {
			$stars .= "★";
		}
		if ($half == 1) {
			$stars .= "⯪";
		}
		for ($i=0; $i < $empty ; $i++) {
			$stars .= "☆";
		}
	}

	return $stars;
}

//	Add stars to the title of RSS feed items
function edent_review_display_stars_in_rss($content){
	//	https://www.wpbeginner.com/wp-tutorials/how-to-add-content-and-completely-manipulate-your-wordpress-rss-feeds/#aioseo-2-adding-additional-text-to-post-titles-in-rss
	global $wp_query;
	if ( null != $wp_query->post ) {
		$post_id = $wp_query->post->ID;
	} else {
		return $content;
	}
	// https://developer.wordpress.org/reference/functions/the_meta/
	$review_data = get_post_meta($post_id, "_edent_review_meta_key", true);
	if ( isset( $review_data["review"] ) )
	{
		$content .= " ";

		if ($review_data["review"] == "book") {
			$review_rating =  $review_data["book"]["rating"];
		} elseif($review_data["review"] == "movie") {
			$review_rating =  $review_data["movie"]["rating"];
		} elseif($review_data["review"] == "game") {
			$review_rating =  $review_data["game"]["rating"];
		} elseif($review_data["review"] == "product") {
			$review_rating =  $review_data["product"]["rating"];
		} elseif($review_data["review"] == "event") {
			$review_rating =  $review_data["event"]["rating"];
		} elseif($review_data["review"] == "restaurant") {
			$review_rating =  $review_data["restaurant"]["rating"];
		}

		//	Display Star Rating in title
		$full = floor($review_rating);
		$half = 0;
		if ($review_rating - $full == 0.5) {
			$half = 1;
		}

		$empty = 5 - $half - $full;

		for ($i=0; $i < $full ; $i++) {
			$content .= "★";
		}
		if ($half == 1) {
			$content .= "⯪";
		}
		for ($i=0; $i < $empty ; $i++) {
			$content .= "☆";
		}
	}
	return $content;
}
add_filter('the_title_rss', 'edent_review_display_stars_in_rss');
