<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

//	Remove mandatory classic theme
function disable_classic_theme_styles() {
	wp_deregister_style( 'classic-theme-styles' );
	wp_dequeue_style(    'classic-theme-styles' );
}
add_filter( 'wp_enqueue_scripts', 'disable_classic_theme_styles', 100 );

//	Completely Remove jQuery From WordPress
//	https://mariushosting.com/how-to-completely-remove-jquery-from-wordpress/
/*
 * function remove_jquery() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', false);
	}
}
add_action('init', 'remove_jquery');
*/

// REMOVE WP EMOJI
//	http://www.denisbouquet.com/remove-wordpress-emoji-code/
remove_action( 'wp_head',             'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles',     'print_emoji_styles' );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles',  'print_emoji_styles' );

// https://wordpress.org/support/topic/remove-the-new-dns-prefetch-code/
add_filter( 'emoji_svg_url', '__return_false' );

//	https://css-tricks.com/snippets/wordpress/disable-automatic-formatting/
remove_filter( 'the_content',  'wptexturize' );
remove_filter( 'the_excerpt',  'wptexturize' );
remove_filter( 'comment_text', 'wptexturize' );
remove_filter( 'the_title',    'wptexturize' );

//	Remove Webmention custom comments
remove_action( 'init', array( '\Webmention\Comment_Walker', 'init' ) );

//	Remove Gutenberg Styles
//	https://wordpress.org/support/topic/how-to-disable-inline-styling-style-idglobal-styles-inline-css/
remove_action( 'wp_enqueue_scripts', 'wp_enqueue_global_styles' );

//	Remove hovercards on comment links in admin area
// https://wordpress.org/support/topic/how-to-disable-mshots-service/#post-12946617
function disable_akismet_mshots( $value ) {
	return false;
}
add_filter( 'akismet_enable_mshots', 'disable_akismet_mshots' );

//	Remove Gutenberg Block Library CSS from loading on the frontend
//	https://smartwp.com/remove-gutenberg-css/
function remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wp-components' );
}
add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css', 100 );

// Remove Unused IndieWeb Widget code
function remove_indieweb_widget_css(){
	wp_dequeue_style( 'indieweb' );
	wp_dequeue_style( 'webmention' );
}
add_action( 'wp_enqueue_scripts', 'remove_indieweb_widget_css', 100 );



// Remove Unused Plugin code
function remove_plugin_css_js(){
	wp_dequeue_style( 'image-sizes' );
}
add_action( 'wp_enqueue_scripts', 'remove_plugin_css_js', 100 );

//	More WordPress crap
add_action('init', function() {
	remove_filter( 'the_content', 'convert_smilies', 20 );
	foreach ( array( 'the_content', 'the_title', 'wp_title', 'document_title' ) as $filter ) {
		remove_filter( $filter, 'capital_P_dangit', 11 );
	}
	remove_filter( 'comment_text', 'capital_P_dangit', 31 );	//	No idea why this is separate
	remove_filter( 'the_content', 'do_blocks', 9 );
}, 11);

//	From https://wordpress.org/plugins/classic-widgets/
//	Disables the block editor from managing widgets in the Gutenberg plugin.
add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' );
//	Disables the block editor from managing widgets.
add_filter( 'use_widgets_block_editor', '__return_false' );

//	Rename Koko file
//	https://github.com/ibericode/koko-analytics/blob/main/code-snippets/use-different-custom-endpoint.php
define( "KOKO_ANALYTICS_CUSTOM_ENDPOINT", "/okok.php" );

//	Remove WordPress forced image size
//	https://core.trac.wordpress.org/ticket/62413#comment:40
add_filter('wp_img_tag_add_auto_sizes', '__return_false');


//	ActivityPub

//	Remove ActivityPub Plugin CSS
function remove_activitypub_block_css(){
	wp_dequeue_style( 'activitypub-followers-style' );
	wp_dequeue_style( 'activitypub-follow-me-style' );
	wp_dequeue_style( 'activitypub-reactions-style' );
}
add_action( 'wp_enqueue_scripts', 'remove_activitypub_block_css', 100 );

//	Remove Fediverse Previews
//	https://github.com/Automattic/wordpress-activitypub/blob/79ce3414b12b6dc80151de22ce10dce8d2cad9ab/includes/class-admin.php#L876
function modify_list_row_actions( $actions, $post ) {
	return array_diff_key( $actions, array( "activitypub" => true ) );
}
add_filter( "post_row_actions", "modify_list_row_actions", 99, 2 );

//	Remove ActivityPub vestigial script
//	https://github.com/Automattic/wordpress-activitypub/issues/1340
function remove_activitypub_options_injection() {
	remove_action( 'wp_head', array( 'Activitypub\Blocks', 'inject_activitypub_options' ), 11 );
}
add_action( "init", "remove_activitypub_options_injection", 15 );
add_filter( "activitypub_site_supports_blocks", "__return_false" );