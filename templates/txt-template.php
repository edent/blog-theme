<?php
/*
Template Name: Plaintext Template
*/

header('Content-Type: text/plain; charset=utf-8'); 

//	Display the blog's name
bloginfo( 'name' );
echo "\n\n";

//	Run the query for the URl requested
$requested_url = $_SERVER['REQUEST_URI'];
$blog_details = wp_parse_url( home_url() );
$query = get_query_for_url( 
	                                                        //	Remove the .txt from the end
	$blog_details["scheme"] . "://" . $blog_details["host"] . substr( $requested_url, 0, -4 )
);

//	Loop through all the posts
if ( $query->have_posts() ) {

	while ( $query->have_posts() ) : $query->the_post();

		//	Display the date
		if ( get_post_type() == 'post' ) {
			the_time( get_option( 'date_format' ) ); 
			echo "\n";
		}

		//	Title and link
		echo get_the_title() . "\n";
		echo get_permalink() . "\n\n";

		//	Render the content through all the embed filters
		$content = apply_filters( 'the_content', get_the_content() );

		//	Replace any images with their alt-text
		$content = replace_img_with_alt( $content );

		//	All the content, decoded then stripped of any HTML
		echo strip_tags(
			html_entity_decode(
				$content,
				ENT_HTML5,
				"UTF-8"
			)
		); 
		echo "\n\n------------------------------";
		echo "\n\n";
	endwhile; 
} 

//	Footer
echo "␃␄";
die();

function replace_img_with_alt( $html ) {
	//	Define the regular expression pattern to match <img> elements
	$pattern = '/<img[^>]+>/';

	//	Perform the replacement using a callback function
	$html = preg_replace_callback( $pattern, function( $match ) {
		//	Extract the alt attribute from the matched <img> element
		preg_match('/alt=["\']([^"\']+)["\']/', $match[0], $alt_matches);
		
		// alt text or empty string if alt attribute is not set
		$alt = isset($alt_matches[1]) ? $alt_matches[1] : ''; 
		$alt = html_entity_decode( $alt );
		
		// Return the alt text
		return "[Image: {$alt}]\n";
	}, $html);

	return $html;
}


function get_query_for_url( $url ) {
	//	Get all the rewrite rules
	global $wp_rewrite;

	//	Get the WordPress site URL path
	$site_path = parse_url( get_site_url(), PHP_URL_PATH ) . "/";

	//	Parse the requested URL
	$url_parts = parse_url( $url );

	//	Remove the domain and site path from the URL
	//	For example, change `https://example.com/blog/2024/04/test` to just `2024/04/test`
	$url_path = isset( $url_parts['path'] ) ? str_replace( $site_path, '', $url_parts['path'] ) : '';

	//	Match the URL against WordPress rewrite rules
	$rewrite_rules = $wp_rewrite->wp_rewrite_rules();
	$matched_rule = false;

	foreach ( $rewrite_rules as $pattern => $query ) {
		if ( preg_match( "#^$pattern#", $url_path, $matches ) ) {
			$matched_rule = $query;
			break;
		}
	}

	//	Replace each occurrence of $matches[N] with the corresponding value
	foreach ( $matches as $key => $value ) {
		$matched_rule = str_replace( "\$matches[{$key}]", $value, $matched_rule );
	}

	//	Turn the query string into a WordPress query
	$query_params = array();
	parse_str(
		parse_url( $matched_rule, PHP_URL_QUERY), 
		$query_params
	);

	//	If nothing matches, it's probably the front page. Get this year's posts.
	if ( empty( $query_params ) ) {
		$query_params["year"] = date("Y");
	}

	//	Construct a new WP_Query object using the extracted query parameters
	$query = new WP_Query($query_params);

	//	Return the result of the query
	return $query;
}