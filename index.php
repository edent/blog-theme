<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;	//	Exit if accessed directly.
}

/*
 *	Tidy the HTML
 *	Forked from https://github.com/Dan-Q/wp-htmltidy-hack-demo
 */

function tidy_entire_page( string $buffer ): string {

	//	Don't tidy logged in users
	if( is_user_logged_in() ) return $buffer;

	// Instantiate a copy of HTMLTidy:
	$tidy = new tidy;

	// Configure HTMLTidy:
	$tidy->parseString( $buffer, [
		"indent"              => 2,       //	Horrible hack to prevent newlines in <p> elements. https://www.php.net/manual/en/tidy.examples.basic.php#107877
		//"indent-with-tabs"  => true,    //	Doesn't work due to https://github.com/htacg/tidy-html5/issues/1107
		"indent-spaces"       => 8,       //	Use 8 spaces which will later be replaced by a tab
		"output-html"         => true,    //	Standard HTML5
		"keep-tabs"           => true,    //	Keep any hard-coded tabs
		"sort-attributes"     => true,    //	Just looks a bit nicer
		"wrap"                => 2048,    //	Lots of long text, don't wrap unless necessary
		"vertical-space"      => false,   //	Not sure if this works
		"tidy-mark"           => false,   //	Remove advert
		"merge-divs"          => false,   //	Don't monkey with divs
		"merge-spans"         => false,   //	Or spans
		"preserve-entities"   => true,    //	Keep hardcoded entities
		"force-output"        => true,    //	Always output, even in case of errors
		"break-before-br"     => true,    //	Always \n before you <br>
		"preserve-entities"   => true,    //	Keep and &#...; entities.
		"new-blocklevel-tags" => "search",//	Not present in older versions of Tidy
		"priority-attributes" => "id,class,name", //	put these attributes first
		"warn-proprietary-attributes" => false,   //	proprietary attributes aren't a cause for concern
	], "utf8" );                          // Ensure we treat the incoming document as UTF-8

	// Repair broken HTML and tidy according to the rules above:
	$tidy->cleanRepair();

	// Replace the 8 spaces with a tab
	$tidy = preg_replace( '/        /', "\t", $tidy );

	// Move inline comments to a newline
	$tidy = preg_replace( '/><!--/', ">\n<!--", $tidy );

	return $tidy;
}

//	Create a buffer and then tidy it
ob_start( "tidy_entire_page" );
//	Because the output bugger is never explicitly finished, it is automatically closed when PHP finishes executing.
//	That is, when all the page content has been produced

//	General options
global $wp;
$current_URl = home_url($wp->request);
$home = esc_url( home_url() );

$blogName        = get_bloginfo( "name" );
$blogDescription = "Regular blogging by Terence Eden.";
$permalink = get_permalink();

$theme = get_theme_file_uri();
$fonts = $theme . "/assets/fonts/";

$themeDirectory = get_template_directory();
$fontDirectory  = $themeDirectory . "/assets/fonts/";
$svgDirectory   = $themeDirectory . "/assets/images/"; 

$postTitle = get_the_title();
$iconAlt   = "Photo of Terence Eden. He has a beard and is smiling.";

$lang   = get_language_attributes();
$locale = get_locale();

echo <<<HTML
<!DOCTYPE html>
<html class="no-js" {$lang} prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport"      content="width=device-width, initial-scale=1.0">
		<meta name="Description"   content="{$blogDescription}">
		<meta name="citation_issn" content="2753-1570">
<!-- Links -->
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="author"  href="https://orcid.org/0000-0002-9265-9069">
		<link rel="me"      href="https://mastodon.social/@Edent">
		<link rel="me"      href="https://github.com/edent">
		<link rel="me"      href="https://gitlab.com/edent">
		<link rel="me"      href="https://twitter.com/edent">
		<link rel="me"      href="https://www.flickr.com/people/edent">
		<link rel="me"      href="https://bsky.app/profile/edent.tel">
		<link rel="me"      href="https://reddit.com/user/edent">
		<link rel="me"      href="https://linkedin.com/in/TerenceEden/">
		<link rel="me"      href="https://keybase.io/edent">
		<link rel="me"      href="https://www.wikidata.org/wiki/Q15733094">
		<link rel="me"      href="https://www.threads.net/@_edent_">
		<link rel="pgpkey"  href="https://keybase.io/edent">
		<link rel="license" href="https://shkspr.mobi/blog/copyright-terence-eden/">
<!-- Feeds & Alternatives -->
		<link rel="alternate" type="application/atom+xml" href="{$home}/feed/atom/" title="Atom Feed.">
		<link rel="alternate" type="application/rss+xml"  href="{$home}/feed"       title="RSS Feed.">
		<link rel="alternate" type="text/plain"           href="{$current_URl}/.txt" title="Text only version.">
<!-- Fonts -->
HTML;

	//	Get every file (not . and .. ) in the font directory
	$fontFiles = array_diff( scandir( $fontDirectory ), array( ".", ".." ) );

	//	Add them to the preload
	foreach ( $fontFiles as $font ) {
		echo <<<EOT
		<link rel="preload" href="{$fonts}{$font}" as="font" type="font/woff2" crossorigin>\n
EOT;
	}

echo <<< HTML
<!-- Icons -->
		<link rel="icon"             type="image/png"  sizes="32x32"   href="/favicon-32x32.png">
		<link rel="icon"             type="image/png"  sizes="16x16"   href="/favicon-16x16.png">
		<link rel="apple-touch-icon" type="image/png"  sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon"             type="image/jpeg" sizes="192x192" href="/favicon-192x192.jpg">
		<link rel="mask-icon"                          color="#5bbad5" href="/safari-pinned-tab.svg">
		<meta name="msapplication-TileImage" content="/favicon-270x270.jpg">
<!-- Theming -->
		<link rel="manifest" href="/site.webmanifest">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#fff">
<!-- Validation -->
		<meta name="msvalidate.01" content="DD30664E807358AF289330BB411A3FA3">
HTML;

//	Set up for metadata
if ( is_single() or is_page() ) {
	$type = "article";
	$thumbnail_url  = get_the_post_thumbnail_url();
	$thumbnail_id   = get_post_thumbnail_id();
	$thumbnail_alt  = get_post_meta( $thumbnail_id, "_wp_attachment_image_alt", true );
	$thumbnail_alt  = htmlentities( $thumbnail_alt, ENT_QUOTES, "UTF-8", false );
	$thumbnail_metadata = wp_get_attachment_metadata( $thumbnail_id );
	if ( $thumbnail_metadata ) {
		$thumbnail_width  = ( isset( $thumbnail_metadata["width"]  ) ? $thumbnail_metadata["width"]  : "");
		$thumbnail_height = ( isset( $thumbnail_metadata["height"] ) ? $thumbnail_metadata["height"] : "") ;
	} else {
		$thumbnail_width  = "";
		$thumbnail_height = "";	
	}
	$ogTitle   = htmlentities( $postTitle, ENT_QUOTES, "UTF-8", false );
	$published = get_the_time( "c" );
	$modified  = get_the_modified_date( "c" );
	$ogURL     = $permalink;
	$ogDescription = htmlentities( get_the_excerpt(), ENT_QUOTES, "UTF-8", false );
} else {
	$type = "website";
	$thumbnail_url = "https://shkspr.mobi/favicon-270x270.jpg";
	$thumbnail_alt = $iconAlt;
	$thumbnail_width  = 270;
	$thumbnail_height = 270;
	$ogTitle   = htmlentities( $blogName, ENT_QUOTES, "UTF-8", false );
	$published = date( "c", strtotime( get_lastpostdate()     ));
	$modified  = date( "c", strtotime( get_lastpostmodified() ));
	$ogURL     = get_site_url();
	$ogDescription = $blogDescription;
}

echo <<< HTML
<!-- Open Graph -->
	<meta property="og:locale"      name="og:locale"           content="{$locale}">
	<meta property="og:site_name"   name="og:site_name"        content="{$blogName}">
	<meta property="og:title"       name="twitter:title"       content="{$ogTitle}">
	<meta property="og:type"                                   content="{$type}">
	<meta property="og:url"                                    content="{$ogURL}">
	<meta property="og:description" name="twitter:description" content="{$ogDescription}">

	<meta property="og:image"       name="twitter:image"       content="{$thumbnail_url}">
	<meta property="og:image:width"                            content="{$thumbnail_width}">
	<meta property="og:image:height"                           content="{$thumbnail_height}">
	<meta property="og:image:alt"   name="twitter:image:alt"   content="{$thumbnail_alt}">

	<meta property="article:published_time"                    content="{$published}">
	<meta property="article:modified_time"                     content="{$modified}">

	<meta property="fediverse:creator"                         content="edent@mastodon.social">
	<meta                           name="twitter:site"        content="@edent">
	<meta                           name="twitter:card"        content="summary">
HTML;

	wp_head();

	echo <<<HTML
	</head>
HTML;
?>
	<body <?php body_class(); ?> itemscope itemtype="https://schema.org/Blog">
		<header class="site-header <?php
		if (is_single() || is_page()) {
			echo "single";
		}?>">
			<?php
				//	CSS Naked Day
				$date_array = localtime(time(), true);
				//	9th April (zero based months)
				if ( $date_array["tm_mday"] == 9 && $date_array["tm_mon"] == 3 )
				{
					?>
						<p>This site isn't broken! It is taking part in <a href="https://css-naked-day.github.io/"><abbr title="Cascading Style Sheets">CSS</abbr> Naked Day</a>.</p>
					<?php
				} else {
					   }
				if ( is_single() ): ?>
				<a href="<?php echo $home; ?>" rel="home" id="header-avatar"><img src="/apple-touch-icon.png" class="avatar" alt="<?php echo $iconAlt; ?>" width="100" height="100"></a>
				<nav class="site-title" itemprop="headline">
					<a href="<?php echo $home; ?>" rel="home"><?php echo $blogName; ?></a>
				</nav>
			<?php
				else: ?>
				<a href="<?php echo $home; ?>" rel="home" id="header-avatar"><img src="/apple-touch-icon.png" class="avatar" alt="<?php echo $iconAlt; ?>" width="100" height="100"></a>
				<h1 class="site-title" itemprop="headline">
					<a href="<?php echo $home; ?>" rel="home"><?php echo $blogName; ?></a>
				</h1>
			<?php
				endif;
			?>
			<aside id="social-links">
				<a href="https://mastodon.social/@edent">
					<img src="<?php echo inline_svg( $svgDirectory . "mastodon.svg"); ?>" alt="Mastodon." width="50"></a>
				<a href="https://linkedin.com/in/TerenceEden">
					<img src="<?php echo inline_svg( $svgDirectory . "linkedin.svg"); ?>" alt="LinkedIn." width="50"></a>
				<a href="https://github.com/edent">
					<img src="<?php echo inline_svg( $svgDirectory . "github.svg");   ?>" alt="GitHub." width="50"></a>
				<a href="https://edent.tel/">
					<img src="<?php echo inline_svg( $svgDirectory . "email.svg");    ?>" alt="Email." width="50"></a>
				<a href="<?php echo $home; ?>/feed/atom">
					<img src="<?php echo inline_svg( $svgDirectory . "rss.svg");      ?>" alt="Feed." width="50"></a>
			</aside>
			<?php
				edent_theme_chooser(); 
			?>
		</header>
		<?php
			//echo '<aside id="top-widgets" class="widget-area">';
			//dynamic_sidebar( 'top-widgets' ); 
			//echo '</aside>';
		?>	
		<?php
		if (is_single() || is_page()) {
			echo "<main itemscope itemprop=\"blogPost\">";
		} else {
			echo "<main class=\"posts\">";
		}

		$postCounter = 0;
		if ( have_posts() )  :
			while ( have_posts() ) : the_post();
				$postID = get_the_ID();
				if ( is_single() || is_page()) {	
					echo "<article ";
						post_class( "post" ); 
					echo ">";

						echo "<header class=\"entry-header\">";
						if (is_page() == false) {
							echo edent_date_svg( get_the_time( "Y-m-d" ) );
						}
						echo "<h1 class=\"entry-title\" itemprop=\"https://schema.org/headline\"><a rel=\"bookmark\" href=\"";
							the_permalink();
							echo "\">";
							the_title(); 
							echo "</a></h1>";
							if ( is_page() == false ) {
								edent_review_display_stars( $postID );
								edent_byline(); 
							}
							echo "<hr class=\"hr-top\">";
						echo "</header>";

						// Thumbnail for Schema
						if ( has_post_thumbnail() ) :
							echo "<a class=\"hidden\" href=\"" . get_the_post_thumbnail_url() . "\" itemprop=\"https://schema.org/image\"></a>";
						endif;

						//	The main body of the blog post
						echo "<div itemprop=\"https://schema.org/articleBody\">";
						the_content();
						echo "</div>";

						// Display any book review
						edent_review_display( $postID );

						//	Don't show our sharing buttons on pages. Only on posts
						if ( is_page() == false ) {
							if ( function_exists( "edent_sharing_buttons" ) ) {
								edent_sharing_buttons( $permalink, $postTitle );
							}
						}
					echo "</article>";

					echo edent_relationship_graph( $postID );
					
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				} else {
					$postLink = get_permalink();
					echo "<article ";
						post_class( "post" ); 
						echo " itemscope itemprop=\"https://schema.org/blogPost\">";
						echo "<header class=\"entry-header\">";
							if (is_page() == false) {
								echo edent_date_svg(get_the_time( "Y-m-d" ));
							}
							echo '<h2 class="entry-title" itemprop="https://schema.org/headline"><a rel="bookmark" href="';
							the_permalink();
							echo '">';
							the_title(); 
							echo "</a></h2>";
							if ( is_page() == false ) {
								edent_review_display_stars( $postID );
								edent_byline(); 
							}
							echo '<hr class="hr-top">';
						echo "</header>";
						if ( has_post_thumbnail() ) :
							if ( $postCounter > 2 ) {
								$loading = "lazy";
							} else {
								$loading = "eager";
							}
							$postCounter++;
							echo '<a class="thumbnail-link" href="' . esc_url( $postLink ) .'">'.	
								get_the_post_thumbnail( $postID, "full", 
									array( "itemprop" => "https://schema.org/image",
									       "loading"  => $loading,
							 		)
								) .
							"</a>";
						endif;
						echo '<p itemprop="https://schema.org/abstract" class="abstract">';
						echo get_the_excerpt();
						echo "</p>";
						echo "<a class=\"moretag\" itemprop=\"https://schema.org/url\" href=\"{$postLink}\">Continue reading →</a>";
					echo "</article>";
				}
			endwhile;

		else :
			// 404
			?>
			<article>
				<h2><?php _e( "Sorry, the page you requested cannot be found.", "edent" ); ?></h2>
				<h2><?php _e( "These are not the blogs you're looking for…",    "edent" ); ?></h2>
				<video height="268" width="480" autoplay loop muted="">
					<source src="<?php echo $theme; ?>/assets/videos/404.mp4">
				</video>
			</article>
		<?php
		endif;
		?>
		</main>
		<?php
		if ( ( ! is_singular() ) && ( $wp_query->found_posts > get_option( "posts_per_page" ) ) ) : 
			$nav_start = <<<EOT
			<aside>
				<nav class="navigation posts-navigation">
					<h2 class="screen-reader-text">Posts navigation</h2>
					
			EOT;
			echo $nav_start;
			if ( "ASC" == get_query_var( "order" ) ) {
				//	Reverse direction if in ascending order
				previous_posts_link( __( "👈 Previous posts", "edent" ) );
				    next_posts_link( __( "Next posts 👉",     "edent" ) );

			} else {
				previous_posts_link( __( "Newer posts 👉", "edent" ) );
				    next_posts_link( __( "👈 Older posts", "edent" ) );
			}
			$nav_end = <<<EOT
				</nav>
			</aside>
			EOT;
			echo $nav_end;
		endif; ?>

		<aside id="secondary" class="widget-area">
			<?php dynamic_sidebar( "sidebar-1" ); ?>
		</aside>

		<footer id="colophon" class="site-footer">
			<nav>
				<?php wp_nav_menu( array( "container" => "" ) ); ?>
				<a href="https://portal.issn.org/resource/ISSN/2753-1570"><abbr title="International Standard Serial Number">ISSN</abbr> <span itemprop="issn">2753-1570</span></a>
				<time id="RomanYear" datetime="<?php echo date("Y"); ?>"><?php echo romanYear(); ?></time>
			</nav>
		</footer>
		<?php
			wp_footer();
		?>
		<?php
			//	CSS Naked Day
			$date_array = localtime(time(), true);
			//	9th April (zero based months)
			if ($date_array["tm_mday"] == 9 && $date_array["tm_mon"] == 3)
			{
				?>
				<script>for (var style in styles = document.styleSheets) styles[style].disabled = true;</script>
				<?php
			}
			
			//	Koko Tracking for non-JS users
			if (is_singular()) {
		?>
		<noscript>
			<!-- Tracking -->
			<img src="/blog/okok.php?nv=0&up=0&p=<?php echo get_queried_object_id(); ?>" alt="" width="0" height="0" class="hidden">
		</noscript>
		<?php } // end if is singular ?>
	</body>
</html>