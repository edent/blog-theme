<search role="search">
	<form method="get" id="searchform" class="searchform" action="<?php echo get_site_url(); ?>">
		<label class="screen-reader-text" for="s">Search for:</label>
		<input type="search" value="" name="s" id="s">
		<input type="submit" id="searchsubmit" value="Search">
	</form>
</search>