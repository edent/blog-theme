<?php

function edent_theme_chooser() {
echo <<<EOT
<label for="theme">&nbsp;⚙ Theme:</label>
<select id="theme" title="Theme Switcher">
	<option selected disabled class="emoji">🌒 / 🌞</option>
	<option value="dark" >🌒 Dark</option>
	<option value="light">🌞 Light</option>
	<option value="eink" >📰 eInk</option>
	<option value="xterm">💻 xterm</option>
	<option value="nude" >👻 Nude</option>
	<option value=""     >♻️ Reset</option>
</select>
<script>
	//	Get the theme switcher
	var themeSelect = document.getElementById('theme');

	//	If a theme has previously been selected, set it
	if ( localStorage.theme ) {
		themeSelect.value = localStorage.theme;

		if ( "nude" == themeSelect.value ) {
			for (var style in styles = document.styleSheets)
			styles[style].disabled = true;
		}

		//	Shim for Firefox
		if ( !CSS.supports("selector(:has(+ *))") ) {
			setTheme(themeSelect.value)
		}
	}

	//	Listen for any changes and save them
	if(themeSelect) {
		themeSelect.addEventListener('change', function(event){
			localStorage.setItem('theme', themeSelect.value);

			if ( "nude" == themeSelect.value ) {
				for (var style in styles = document.styleSheets)
				styles[style].disabled = true;
			} else {
				for (var style in styles = document.styleSheets)
				styles[style].disabled = false;
			}

			//	Shim for Firefox
			if ( !CSS.supports("selector(:has(+ *))") ) {
				setTheme(themeSelect.value)
			}

		});
	}

	//	Manually set theme - only if browser doesn't support :has()
	function setTheme(themeValue) {
		var root = document.querySelector(':root');
		if ("dark" == themeValue) {
			root.style.setProperty("--color-a",     "var(--color-a-D)");
			root.style.setProperty("--color-bg",    "var(--color-bg-D)");
			root.style.setProperty("--color-bg-secondary", "var(--color-bg-secondary-D)");
			root.style.setProperty("--color-box",   "var(--color-box-D)");
			root.style.setProperty("--color-text",  "var(--color-text-D)");
			root.style.setProperty("--color-input", "var(--color-input-D)");
			root.style.setProperty("--color-text-secondary", "var(--color-text-secondary-D)");
			root.style.setProperty("--font-family", "var(--font-family-sans)");
			document.getElementsByTagName("html")[0].style.filter = "saturate(.66)";
		}
		if ("light" == themeValue) {
			root.style.setProperty("--color-a",     "var(--color-a-L)");
			root.style.setProperty("--color-bg",    "var(--color-bg-L)");
			root.style.setProperty("--color-bg-secondary", "var(--color-bg-secondary-L)");
			root.style.setProperty("--color-box",   "var(--color-box-L)");
			root.style.setProperty("--color-text",  "var(--color-text-L)");
			root.style.setProperty("--color-input", "var(--color-input-L)");
			root.style.setProperty("--color-text-secondary", "var(--color-text-secondary-L)");
			root.style.setProperty("--font-family", "var(--font-family-sans)");
			document.getElementsByTagName("html")[0].style.filter = "saturate(1)";
		}
		if ("eink" == themeValue) {
			root.style.setProperty("--color-a",     "var(--color-a-L)");
			root.style.setProperty("--color-bg",    "var(--color-bg-L)");
			root.style.setProperty("--color-bg-secondary", "var(--color-bg-secondary-L)");
			root.style.setProperty("--color-box",   "var(--color-box-L)");
			root.style.setProperty("--color-text",  "var(--color-text-L)");
			root.style.setProperty("--color-input", "var(--color-input-L)");
			root.style.setProperty("--color-text-secondary", "var(--color-text-secondary-L)");
			root.style.setProperty("--font-family", "var(--font-family-sans)");
			document.getElementsByTagName("html")[0].style.filter = "saturate(0)"; 
		}
		if ("xterm" == themeValue) {
			root.style.setProperty("--color-a",     "lime");
			root.style.setProperty("--color-bg",    "black");
			root.style.setProperty("--color-bg-secondary", "black");
			root.style.setProperty("--color-box",   "black");
			root.style.setProperty("--color-text",  "lime");
			root.style.setProperty("--color-input", "black");
			root.style.setProperty("--color-text-secondary", "lime");
			root.style.setProperty("--font-family", "var(--font-family-mono)");
			document.getElementsByTagName("html")[0].style.filter = "saturate(1)"; 
		}
		if ("" == themeValue) {
			//	Reset was selected
			location.reload();
		}
	}
</script>
EOT;
}