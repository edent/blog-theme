<?php

/**
 * Custom comment layout
 * https://ralphjsmit.com/how-to-output-custom-html-for-wordpress-comments
 */
function edent_comments_walker() {

	//	Basic comment data
	$comment_id = get_comment_id();
	$comment    = get_comment( $comment_id );
	$comment_type = $comment -> comment_type;

	if ( $comment_type == "pingback" || $comment_type == "trackback" ) {
		//	Handled in edent_pings_walker
		return;
	}

	//	For debug purposes
	//	$comment_debug = print_r($comment, true);
	// error_log($comment_debug, 0);

	//	Date the comment was submitted
	$comment_date        = get_comment_date( "c" );
	//	In slightly more human-readable format
	$comment_date_human  = get_comment_date( "Y-m-d H:i" );

	//	Author Details
	$comment_author      = get_comment_author();

	//	Author's URl if they've added one, or from WebMention
	$comment_author_url  = get_comment_author_url();

	//	If there's an Author URl, link it
	if ( $comment_author_url != null ) {
		$comment_author_name = "<a itemprop='url' href='{$comment_author_url}' rel='external nofollow ugc' class='url'>{$comment_author}</a>";
	} else {
		$comment_author_name = "{$comment_author}";
	}

	//	If this is a WebMention, a separate URL will be set
	$comment_url = get_comment_meta( $comment->comment_ID, 'url', true );

	//	If the Comment URl is different to the Author's URl, it is probably a WebMention - link to the external site for the user's link
	if ($comment_url != null && $comment_url != $comment_author_url) {
		$comment_author_name = "<a itemprop='url' href='{$comment_url}' rel='external nofollow ugc' class='url'>{$comment_author}</a>";
	}
	
	//	Permalink - does both internal and external comments
	$comment_link = get_comment_link( $comment_id );

	//	Internal anchor to the comment
	$comment_url_link = "<a href='{$comment_link}' itemprop='url'>{$comment_date_human}</a>";
	
	//	Better Pingback / WebMention
	$comment_protocol = get_comment_meta( $comment->comment_ID, 'protocol', true );
	
	if ( "webmention" == $comment_protocol ) {
		$author_type = "Person";
		$comment_url_host = parse_url($comment_url, PHP_URL_HOST);

		if ( null != $comment_url_host ) {
			$comment_url_ico = "<img src=\"https://icons.duckduckgo.com/ip9/{$comment_url_host}.ico\" class=\"pingback-favicon no-border-radius\" alt=\"\" loading=\"lazy\">";
		} else {
			$comment_url_ico = "";
		}
		
		$says = "<span class=\"fn\" itemprop=\"name\">$comment_url_ico $comment_author_name</span> ";

		$comment_text		= apply_filters( 'comment_text', get_comment_text(), $comment);

		//	Truncate anything too long
		$text_length = mb_strlen( wp_strip_all_tags( html_entity_decode( $comment_text, ENT_QUOTES ) ) );
		if ( $text_length >= 1024 ) {
			$says .= "mentioned this.";
			$comment_text = "<a href=\"$comment_url\" target=\"_blank\">Read more</a>.";
		} else {
			$says .= "said on {$comment_url_host}:";
			$comment_text		= apply_filters( 'comment_text', get_comment_text(), $comment);
		}

	} else {
		$comment_type = $comment -> comment_type;
	
		if ( "pingback" == $comment_type ) {
			$author_type = "Organization";
			$comment_url_host = parse_url($comment_author_url, PHP_URL_HOST);
			if ( null != $comment_url_host ) {
				$comment_url_ico = "<img src=\"https://icons.duckduckgo.com/ip9/{$comment_url_host}.ico\" class=\"pingback-favicon no-border-radius\" alt=\"\" loading=\"lazy\">";
			} else {
				$comment_url_ico = "";
			}

			$says = "Pingback from <span class=\"fn\" itemprop=\"name\">$comment_url_ico $comment_author_name</span>:";
		} else {
			$author_type = "Person";
			$comment_url_host = parse_url($comment_author_url, PHP_URL_HOST);

			if ( null != $comment_url_host ) {
				$comment_url_ico = "<img src=\"https://icons.duckduckgo.com/ip9/{$comment_url_host}.ico\" class=\"pingback-favicon no-border-radius\" alt=\"\" loading=\"lazy\">";
			} else {
				$comment_url_ico = "";
			}
			
			$says = "<span class=\"fn\" itemprop=\"name\">$comment_url_ico $comment_author_name</span> says:";
		}
		$comment_text		= apply_filters( 'comment_text', get_comment_text(), $comment);
	}

	//	Author's Avatar based on ID
	//	As per https://developer.wordpress.org/reference/functions/get_avatar/ both default & alt must be set
	$gravatar			= get_avatar( $comment, 64, "", "", array('extra_attr' => 'itemprop="image" loading="lazy"') );

	//	Comment needs newlines added
	// $comment_text		= nl2br(get_comment_text());
	// $comment_text		= apply_filters( 'comment_text', get_comment_text(), $comment);

	//	The comment may have various classes. They are stored as an array
	$comment_classes      = get_comment_class();
	$comment_classes_text = "";
	foreach( $comment_classes as $class ) {
		$comment_classes_text .= $class . " ";
	}
	$comment_classes_text = trim( $comment_classes_text );

	//	Link to open the reply box
	$comment_reply_link = get_comment_reply_link( [
					'depth'	   =>  20,
					'max_depth' => 100,
					'before'	 => '',
					'after'	 => ''
			] );

	//	Fediverse comments get a special reply-to link
	if ( !str_starts_with( $comment_link, "https://shkspr.mobi/blog" ) ) {
		$comment_reply_link .= " | <a href=\"{$comment_link}\" target=\"_blank\">Reply to original comment on {$comment_url_host}</a>";
	}

	//	Write the comment HTML
	echo <<<EOT
	<li id="comment-$comment_id" itemscope itemtype="https://schema.org/Comment" itemid="#comment-$comment_id" class="$comment_classes_text">
		<article class="comment-body" id="div-comment-$comment_id">
			<time datetime="$comment_date" class="comment-meta commentmetadata" itemprop="dateCreated">
				$comment_url_link
			</time>
			<div class="comment-author vcard" itemprop="https://schema.org/author" itemscope itemtype="https://schema.org/$author_type">
				$gravatar
				<h3 class="says">$says</h3>
			</div>
			<div itemprop="text" class="comment-text">$comment_text</div>
			$comment_reply_link
		</article>
	EOT;
}

function edent_pings_walker() {

	//	Basic comment data
	$comment_id = get_comment_id();
	$comment    = get_comment( $comment_id );
	// $comment_protocol = get_comment_meta( $comment->comment_ID, 'protocol', true );
	$comment_type = $comment -> comment_type;

	// if ( $comment_type != "pingback" || $comment_type != "trackback" ) {
	// 	return;
	// }

	//	For debug purposes
	//	$comment_debug = print_r($comment, true);
	// error_log($comment_debug, 0);

	//	Date the comment was submitted
	$comment_date        = get_comment_date( "c" );
	//	In slightly more human-readable format
	$comment_date_human  = get_comment_date( "Y-m-d H:i" );

	//	Author Details
	$comment_author      = get_comment_author();

	//	Author's URl if they've added one, or from WebMention
	$comment_author_url  = get_comment_author_url();

	//	If there's an Author URl, link it
	if ( $comment_author_url != null ) {
		$comment_author_name = "<a itemprop='url' href='{$comment_author_url}' rel='external nofollow ugc' class='url'>{$comment_author}</a>";
	} else {
		$comment_author_name = "{$comment_author}";
	}

	//	If this is a WebMention, a separate URL will be set
	$comment_url = get_comment_meta( $comment->comment_ID, 'url', true );

	//	If the Comment URl is different to the Author's URl, it is probably a WebMention - link to the external site for the user's link
	if ($comment_url != null && $comment_url != $comment_author_url) {
		$comment_author_name = "<a itemprop='url' href='{$comment_url}' rel='external nofollow ugc' class='url'>{$comment_author}</a>";
	}
	
	//	Internal anchor to the comment
	$comment_url_link = "<a href='#comment-{$comment_id}' itemprop='url'>{$comment_date_human}</a>";
		
	$author_type = "Organization";
	$comment_url_host = parse_url($comment_author_url, PHP_URL_HOST);
	if ( null != $comment_url_host ) {
		$comment_url_ico = "<img src=\"https://icons.duckduckgo.com/ip9/{$comment_url_host}.ico\" class=\"pingback-favicon no-border-radius\" alt=\"\" loading=\"lazy\">";
	} else {
		$comment_url_ico = "";
	}
	
	$comment_text		= apply_filters( 'comment_text', get_comment_text(), $comment);

	$says = "<span class=\"fn\" itemprop=\"name\">$comment_url_ico $comment_author_name</span>";

	//	Not all pingbacks have text. If not, just show the link
	if ( $comment_text != null ) {
		$says .= ":";
		$comment_text_html = "<div itemprop=\"text\" class=\"comment-text\">{$comment_text}</div>";
	} else {
		//	Non-empty text needed to stop Google from complaining.
		$comment_text_html = "<span itemprop=\"text\">📖</span>";
	}

	//	The comment may have various classes. They are stored as an array
	$comment_classes      = get_comment_class();
	$comment_classes_text = "";
	foreach( $comment_classes as $class ) {
		$comment_classes_text .= $class . " ";
	}
	$comment_classes_text = trim( $comment_classes_text );

	//	Write the comment HTML
	echo <<<EOT
	<li id="comment-$comment_id" itemscope itemtype="https://schema.org/Comment" itemid="#comment-$comment_id" class="$comment_classes_text">
		<article class="comment-body" id="div-comment-$comment_id">
			<time datetime="$comment_date" class="comment-meta commentmetadata" itemprop="dateCreated">
				$comment_url_link
			</time>
			<div class="comment-author" itemprop="https://schema.org/author" itemscope itemtype="https://schema.org/$author_type">
				<h3 class="says">$says</h3>
			</div>
			$comment_text_html
		</article>
	EOT;
}

function edent_internal_links_walker( $search_url ) {
	//	Get all the posts which link to this one, oldest first
	//	Exclude the "12,000" comments post as people keep hitting that accidentally
	$the_query = new WP_Query(
		array(
			'post__not_in' => array(46036),
			's' => $search_url,
			'post_type' => 'post',
			"posts_per_page" => "-1",
			"order" => "ASC"
		)
	);

	//	Nothing to do if there are no inbound links
	if ( !$the_query->have_posts() ) {
		return;
	}
	
	echo <<<EOT
	<h3 class="comment-reply-title">What links here from around this blog?</h3>
	<ol class="related-posts">
EOT;
	//	Loop through the posts
	while ( $the_query->have_posts() ) {
		//	Set it up
		$the_query->the_post();
		$id                  = get_the_ID();
		$title               = esc_html( get_the_title() );
		$url                 = get_the_permalink();
		$comment_date        = get_the_date( "c" );
		//	In slightly more human-readable format
		$comment_date_human  = get_the_date( "Y" );

		//	Get the thumbnail
		if ( has_post_thumbnail( $id) ) {
			$thumb = get_the_post_thumbnail( $id, 'full', 
				array( "class"   => "related-post-img",
						 "loading" => "lazy" //	Lazy loading and other attributes
				) 
			);
		} else {
			$thumb = null;
		}

		//	Create the HTML for the related post
		echo <<<EOT
		<li class="related-post">
			<a href="$url">
				$thumb
				$title
			</a>
			<time datetime="$comment_date">$comment_date_human</time>
		</li>
		EOT;

	}
	echo <<<EOT
	</ol>
EOT;
}