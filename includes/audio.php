<?php

function edent_audio_shortcode_override( $html, $attr ) {
	$audio_url = "";

	//	The shortcode will have an attribute with the URl to the file:
	//	e.g. [ audio mp3="https://example.com/path/to/file.mp3" ]
	if ( isset( $attr['wav']  ) ) { $audio_url = $attr['wav'];  }
	if ( isset( $attr['mp3']  ) ) { $audio_url = $attr['mp3'];  }
	if ( isset( $attr['m4a']  ) ) { $audio_url = $attr['m4a'];  }
	if ( isset( $attr['ogg']  ) ) { $audio_url = $attr['ogg'];  }
	if ( isset( $attr['opus'] ) ) { $audio_url = $attr['opus']; };
	if ( isset( $attr['flac'] ) ) { $audio_url = $attr['flac']; };
	if ( isset( $attr['src']  ) ) { $audio_url = $attr['src'];  }; //	For raw links without shortcode

	//	Defaults
	$audio_meta  = null;
	$audio_thumb = false;

	//	Get the attachment ID from the media library
	$audio_id = attachment_url_to_postid( $audio_url );
	if ($audio_id > 0) { 
		$audio_meta  = wp_get_attachment_metadata($audio_id); 
		$audio_thumb = get_the_post_thumbnail_url( $audio_id );
	} else {
		//	Is this on our server?
		$upload_dir = wp_upload_dir(); // Get the upload directory info
		$upload_baseurl = $upload_dir['baseurl'];
		
		// Check if the provided URL belongs to the site
		if (strpos($audio_url, $upload_baseurl) !== false) {
			// URL belongs to the site, get the file path
			$file_path = str_replace($upload_baseurl, $upload_dir['basedir'], $audio_url);
			// Check if the file exists on disk
			if (file_exists($file_path)) {
				require_once ABSPATH . 'wp-admin/includes/media.php';
				$audio_meta  = wp_read_audio_metadata( $file_path );
				$audio_thumb = false;
			}
		}
	}

	//	Get the title of the audio - or use a default
	if ( isset( $audio_meta["title"] ) ) { 
		$audio_title = "🔊 " . htmlspecialchars( $audio_meta["title"], ENT_NOQUOTES | ENT_HTML5 | ENT_SUBSTITUTE, 'UTF-8', /*double_encode*/false ); 
	} else {
		$audio_title = "🔊";
	}

	//	Get the artist of the audio - or use a default
	if ( isset( $audio_meta["artist"] ) && $audio_meta["artist"] != "" ) { 
		$audio_artist = "<br>🎤 " . htmlspecialchars( $audio_meta["artist"], ENT_NOQUOTES | ENT_HTML5 | ENT_SUBSTITUTE, 'UTF-8', /*double_encode*/false ); 
	} else {
		$audio_artist = "";
	}

	//	Set the HTML for the thumbnail
	if ( $audio_thumb ) {
		$audio_thumb_html = "<img src=\"{$audio_thumb}\" class=\"audio-thumb\" alt=\"\">";
	} else {
		$audio_thumb_html = "";
	}

	$audio_html = <<<EOT
<figure class="audio">
	<figcaption class="audio">{$audio_title}{$audio_artist}</figcaption>
	{$audio_thumb_html}
	<audio class="audio-player" controls src="{$audio_url}">
		<p>💾 <a href="{$audio_url}">Download this audio file</a>.</p>
	</audio>
</figure>
EOT;
	return $audio_html;
}
add_filter('wp_audio_shortcode_override', 'edent_audio_shortcode_override', 10, 2);