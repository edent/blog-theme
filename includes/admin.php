<?php

function edent_priority_dashboard_widget_contents() {
	global $wp_filter; 
	//	Change this to the hook you're interested in
	$hook_name = "the_content";
	if ( isset( $wp_filter[$hook_name] ) ) {

		//  Display the hook name in the widget
		echo "<h3>{$hook_name}</h3>";

		//  Start a list
		echo "<ul>";

		//  Loop through the callbacks in priority order
		foreach ( $wp_filter[$hook_name]->callbacks as $priority => $callbacks ) {
			echo "<li>Priority: {$priority}<ul>";

			foreach ( $callbacks as $callback ) {
				//	Some callbacks are arrays
				if ( is_array( $callback["function"] ) ) {
					if (is_object($callback["function"][0])) {
						$callback_info = get_class($callback["function"][0]) . '::' . $callback["function"][1];
					} else {
						$callback_info = $callback["function"][0] . '::' . $callback["function"][1];
					}
				} else {
					$callback_info = $callback["function"];
				}
				//	Show the information
				echo "<li>Callback: {$callback_info}</li>";
			}
			echo "</ul></li>";
		}
		echo '</ul>';

	} 
	else {
		echo "No filters found for hook: {$hook_name}";
	}

	//	Scrap of CSS to ensure list items display properly on the dashboard
	$priority_css_code = "#edent_dashboard_widget ul { list-style: circle; padding: 1em; }";
	//	Inline the CSS
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"data:text/css;base64," . 
		base64_encode($priority_css_code) . "\">";

}

//	Register the widget with the admin dashboard
function edent_register_dashboard_widget() {
	wp_add_dashboard_widget(
		"edent_dashboard_widget",	//	ID of the widget
		"Priorities",	//	Title of the widget
		"edent_priority_dashboard_widget_contents"	//	Function to run
	);
}
add_action( "wp_dashboard_setup", "edent_register_dashboard_widget" );

//	Admin view - change date format
function rfc3339_post_date_time( $time, $post ) {
	//	Modify the default time format
	$rfc3339_time = date( "Y-m-d H:i", strtotime( $post->post_date ) );
	return $rfc3339_time;
}
add_filter( "post_date_column_time", "rfc3339_post_date_time", 10, 2 );