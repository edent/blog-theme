<?php 

function inline_svg( $filename ) {
	//	https://codepen.io/tigt/post/optimizing-svgs-in-data-uris

	if ( file_exists( $filename ) ) {
		if ( "image/svg+xml" == mime_content_type( $filename ) ) {
			$svg = file_get_contents( $filename );

			//	Remove newlines and tabs
			$original = array("\t", "\n");
			$encoded  = array(""  , " ");

			//	Encode
			$encoded_svg = rawurlencode( str_replace( $original, $encoded, $svg ) );

			$prefix = "data:image/svg+xml;charset=utf-8,";

			return $prefix . $encoded_svg;
		}
	}
	return null;
}