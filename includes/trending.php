<?php
//	Create a /trending page
//	Also /trending/week trending/year etc

//	Add rewrite rule for /trending and /trending/{something}
add_action('init', function () {
	add_rewrite_rule(
		'^trending(?:/([^/]+))?/?$',	//	Matches /trending and /trending/{something}
		'index.php?pagename=trending&trending_param=$matches[1]', //	Maps to 'pagename=trending' with optional 'trending_param'
		'top'
	);
});

//	Register custom query variable
add_filter('query_vars', function ($vars) {
    $vars[] = 'trending_param'; // Add 'trending_param' to recognized query vars
    return $vars;
});

// Register the shortcode
function edent_trending_shortcode() {
	global $wpdb;
	flush_rewrite_rules();

	$site = get_site_url();

	$interface = <<<EOT
The most read posts in the last <a href="{$site}/trending/day">day</a>, or <a href="{$site}/trending/week">7 days</a>, or <a href="{$site}/trending/month">28 days</a>, or <a href="{$site}/trending/year">year</a>, or of <a href="{$site}/trending/">all time</a>.
EOT;
	
	$type = strtolower( get_query_var( "trending_param" ) );

	$title = "<h2>Most Read Posts ";

	switch ( $type ) {
		case "day":
			$title .= "Today";
			$interval = 0;
			break;
		case "week":
			$title .= "in the Last 7 Days";
			$interval = 7;
			break;
		case "month":
			$title .= "in the Last Month";
			$interval = 28;
			break;
		case "year":
			$title .= "in the Last Year";
			$interval = 365;
			break;
		default:
			$title .= "of All Time";
			$interval = (365*20);
	}

	$title .= "</h2>";

	// Run a simple MySQL query using $wpdb
	$results = $wpdb->get_results(
"SELECT 
    id,
    SUM(pageviews) AS total_pageviews
FROM 
    wp_koko_analytics_post_stats
WHERE 
    id != 0
    AND date >= CURDATE() - INTERVAL {$interval} DAY
GROUP BY 
    id
ORDER BY 
    total_pageviews DESC
LIMIT 12 OFFSET 0;", 
ARRAY_A);

	if (!empty($results)) {
		$top_posts_ids = array();
		foreach ($results as $row) {
			$top_posts_ids[] = $row["id"]; 
		}
	}
	
	$top_posts = get_posts(
		array( 
			"include" => $top_posts_ids,
			"orderby"   => "post__in"	//	Keep the order supplied
		) 
	);
	$output = <<<EOT
<table>
<thead>
<tr>
  <th align="right">Page Views</th>
  <th>Title</th>
</tr>
</thead>
<tbody>
EOT;

	$counter = 0;
	foreach ($top_posts as $top_post) {
		$output .= '<tr><td align="right">' .number_format( $results[$counter]["total_pageviews"] ) . '</td>';
		$output .= '<td><a href="' . get_permalink( $top_post->ID ) . '">' . htmlspecialchars( $top_post->post_title ) . '</a></td></tr>';
		$counter++;
	}

	$output .= "</tbody></table>";

	return $title . $output . $interface ;
}
add_shortcode('edent_trending', 'edent_trending_shortcode');