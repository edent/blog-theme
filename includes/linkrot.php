<?php
//	Create a /linkrot page

// Register the shortcode
function edent_linkrot_shortcode() {
	global $wpdb;

	//	Placeholder for Output
	$output = "";

	//	Adjust the latest year
	$endYear = date("Y");

	//	Loop through all the years
	for ( $year = 2008; $year <= $endYear; $year++ ) {
		//  Initialise the counters
		$total_links   = 0;
		$archive_links = 0;

		//	This is a slow operation. See if this year has been stored in a transient
		if ( false === ( get_transient( "edent_linkrot_" . $year . "_total" ) ) ) {
		
			//  Get all the published posts for that year
			$posts = get_posts( [
				"date_query" => [
					[ "year" => $year, ],
				],
				"post_type"   => "post",
				"post_status" => "publish",
				"numberposts" => -1,
			] );

			//  Loop through each post
			foreach ( $posts as $post ) {
				$ID = $post->ID;

				//  Render the content of the post
				$content = apply_filters( "the_content", get_the_content( null, false, $ID ) );

				//  Count total occurrences of "<a href="
				preg_match_all( '/<a href=/i', $content, $matches );
				$total_links += count( $matches[0] );

				//  Count total occurrences of "<a href=" which contain "archive.org"
				preg_match_all( '/<a href=["\'][^"\']*archive.org/i', $content, $archive_matches );
				$archive_links += count( $archive_matches[0] );
			}

			//	Save the data as transients
			set_transient(  "edent_linkrot_" . $year . "_total",   $total_links,   rand(1, 4) * WEEK_IN_SECONDS );
			set_transient(  "edent_linkrot_" . $year . "_archive", $archive_links, rand(1, 4) * WEEK_IN_SECONDS );
		}
	}

	//	Table layout
	$table = <<<TAB
	<table style="text-align:right;">
		<thead>
			<tr>
				<th>Year</th>
				<th>Total Links</th>
				<th>Live Links</th>
				<th>Dead Links</th>
				<th>%age Dead</th>
			</tr>
		</thead>
		<tbody>
TAB;

	//	Holder for the JS data
	$data = "";
	//	Holder for running total
	$count_total   = 0;
	$count_archive = 0;
	for ( $year = 2008; $year <= $endYear; $year++ ) {
		$total_links   = get_transient( "edent_linkrot_" . $year . "_total" );
		$archive_links = get_transient( "edent_linkrot_" . $year . "_archive" );
		$live_links    = $total_links - $archive_links;
		$percentage = number_format( ( $archive_links / $total_links ) * 100, 2);

		$table .= "<tr>" . 
			"<td>{$year}</td>".
			"<td>". number_format($total_links,   0) . "</td>".
			"<td>". number_format($live_links,    0) . "</td>".
			"<td>". number_format($archive_links, 0) . "</td>".
			"<td>{$percentage}%</td>".
		"</tr>";
		$data .= "['{$year}', {$archive_links}, {$live_links}],";

		$count_total   += $total_links;
		$count_archive += $archive_links;
	}

	$table .= "</tbody></table>";

	$percentage = number_format( ($count_archive / $count_total) * 100, 2 );

	$js = <<< EOJS
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			var data = google.visualization.arrayToDataTable([
				['Year', "Archived Links", 'Working Links'],
				$data
			]);

			var options = {
				title: 'Linkrot on this Blog',
				chartArea: {width: '75%'},
				height: 500,
				hAxis: {
					title: 'Number of links in posts',
					minValue: 0
				},
				vAxis: {
					title: 'Year'
				},
				colors: ['#f00', '#00f'],
				isStacked: true
			};

			var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

			chart.draw(data, options);
		}
	</script>
	<div id="chart_div"></div>
EOJS;

	return "<h2>Link Rot levels are {$percentage}%</h2><h3>Data</h3>{$table}<h3>Chart</h3>{$js}";
}
add_shortcode( "edent_linkrot", "edent_linkrot_shortcode" );