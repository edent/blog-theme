<?php
//	Remove Bridgy likes and reposts
add_action( 'comment_post', 'check_comment_content', 10, 2 );
function check_comment_content( $comment_id, $comment_approved ) {
	$comment = get_comment( $comment_id );
	// Check if the comment is a like or repost
	if ( $comment->comment_type == "like" || $comment->comment_type == "repost" ) {
		// Set the comment status to 'trash'
		wp_delete_comment( $comment_id );
		//	Stop processing
		die();
	}
}

//	https://jan.boddez.net/wordpress/share-on-mastodon
add_filter( 'share_on_mastodon_status', function( $status, $post ) {
	//	Create a short preview of the post
	$status_preamble = "🆕 blog! ";
	$status_title    = "“" . html_entity_decode( get_the_title($post) ) . "”\n";
	$status_stars    = edent_review_get_stars($post->ID) . "\n";

	//	Don't use the regular get_the_excerpt() because linebreaks need to be preserved
	
	//	Get the plain text without HTML elements
	//	Get the post's full HTML
	$content = apply_filters( "the_content", get_the_content( null, false, $post->ID ) );

	//	Load it into a DOM for manipulation
	libxml_use_internal_errors(true);	//	Disable invalid HTML warnings
	$fullText = new DOMDocument();
	//	Add boilerplate to ensure Unicode is handled properly
	$fullText->loadHTML( '<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body>' . $content );

	//	Remove any footnotes (<sup> elements and their contents)
	$xpath = new DOMXPath($fullText);
	foreach($xpath->evaluate("//sup") as $node) { $node->parentNode->removeChild($node); }
	//	Get the plain text without HTML elements
	$excerpt  = $fullText->textContent;

	//	Trim the status to 500 characters.
	$excerpt = mb_substr( $excerpt, 0, 500 );

	$status_excerpt  = "\n" . $excerpt;

	//	Add a link
	$status_link     = "\n\n👀 Read more: " . get_permalink( $post );

	//	Add tags
	$tags = get_the_tags( $post->ID );
	$status_tags = "";

	if ( $tags ) {
		//	Add a fake <hr>
		$status_tags .= "\n⸻\n";

		foreach ( $tags as $tag ) {
			$status_tags .= '#' . preg_replace( '/\s/', '', $tag->name ) . ' ';
		}
	}

	//	Construct the status elements
	$status_start = $status_preamble . $status_title . $status_stars;
	$status_end   = $status_link . $status_tags;

	//	Max length 500
	$max_characters = 500;
	$total_characters = 0;

	//	How long is it so far?
	$total_characters = mb_strlen( $status_start ) + mb_strlen( $status_end );

	//	Trim the status to the remaining characters. Lose a few for good luck!
	$status_excerpt = mb_substr( $status_excerpt, 0, ($max_characters - $total_characters - 10) );

	//	Don't truncate in the middle of a word
	if ( substr( $status_excerpt, -1 ) !== " ") {
		$lastSpace = strrpos( $status_excerpt, " " );
		if ( $lastSpace !== false ) {
			$status_excerpt = mb_substr( $status_excerpt, 0, $lastSpace );
		}
	}

	//	Add the …
	$status_excerpt .= "…";

	//	Construct the status
	$status = $status_start . $status_excerpt . $status_end;

	//	Go time!
	return $status;
}, 10, 2 );