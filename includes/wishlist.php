<?php
/*
Description: Wishlist page
*/
function edent_wishlist_shortcode() {

	$wishlistJSON = file_get_contents( plugin_dir_path( __FILE__ ) . "wishlist.json");
	$wishlist = json_decode( $wishlistJSON, true );

	$output = "<ul class='related-posts'>";
	foreach ( $wishlist as $book ) {

		isset( $book["img"] )    ? $img    = $book["img"]    : $img    = null;
		isset( $book["title"] )  ? $title  = $book["title"]  : $title  = null;
		isset( $book["author"] ) ? $author = $book["author"] : $author = null; 
		isset( $book["price"] )  ? $price  = $book["price"]  : $price  = null;
		isset( $book["note"] )   ? $note   = $book["note"]   : $note   = null;
		isset( $book["link"] )   ? $link   = $book["link"]   : $link   = null;

		$output .= "<li class='related-post' style='min-width: 9em !important;'>";
			$output .= "<a href='{$link}'>";

				$output .= "<img src='{$img}' width='100' loading='lazy' alt='Book Cover' style='height: auto !important;'>";
		
			$output .= "</a>";
			$output .= "<a href='{$link}'>{$title}</a> {$author}";
			$output .= "<br><strong>{$note}</strong>";
	
		$output .= "</li>";
	}
	$output .= "</ul>";

	return $output;
}

//	Set up the shortcode
function edent_wishlist_shortcode_init() {
	add_shortcode( 'edent_wishlist', 'edent_wishlist_shortcode' );
}
add_action( 'init', 'edent_wishlist_shortcode_init' );
