<section id="comments" class="comments-area">
<?php if ( have_comments() ) : ?>
	<h2 class="comment-reply-title">
		<?php
			printf( // WPCS: XSS OK.
				esc_html(
					_nx(
						'One thought on &ldquo;%2$s&rdquo;',
						'%1$s thoughts on &ldquo;%2$s&rdquo;',
						get_comments_number(),
						'comments title',
						'edent_simple' )
					),
				number_format_i18n( get_comments_number() ),
				get_the_title()
			);
		?>
	</h2>
	<ol class="comment-list">
		<?php
			wp_list_comments( array(
				'style'       => 'ol',
				'short_ping'  => true,
				'avatar_size' => 64,
				'callback'    => 'edent_comments_walker',
				'type'        => 'all'
			) );

			$mastodon_url = esc_url( get_post_meta( get_the_ID(), '_share_on_mastodon_url', true ) );
			if ( ! empty( $mastodon_url ) ) {
				$assets = get_template_directory_uri() . "/assets/images/";
				$b64_mastodon = base64_encode( file_get_contents($assets . "/mastodon.svg") );
				//	TODO - fix comment class hardcoding
				echo <<<EOT
				<li id="comment-mastodon" class="comment even thread-even depth-1">
					<article class="comment-body" id="div-comment-mastodon">
						<div class="comment-author">
							<img class="avatar avatar-64 local-avatar" alt="" src="data:image/svg+xml;base64,$b64_mastodon" loading="lazy" decoding="async" width="64" height="64">
						</div>
						<h3 class="says"><a href="$mastodon_url">More comments on Mastodon</a>.</h3>
					</article>
				EOT;
			}
		?>
	</ol>
<?php endif;

//	ID of current post
//	Prevents being overwritten by links_walker
$postID = get_the_ID();

//	Trackbacks and Pingbacks
//	Only ones which have been published
if ( count ( get_comments( array( 
		"post_id" => $postID, 
		"type" => "pings", 
		"status" => "approve" ) ) 
	) > 0 ) : ?>
	<h3 class="comment-reply-title">Trackbacks and Pingbacks</h3>
	
	<ol class="comment-list">
		<?php
			wp_list_comments( array(
				'style'        => 'ol',
				'short_ping'   => true,
				'avatar_size'  => 64,
				'callback'     => 'edent_pings_walker',
				"type"         => "pings"
			) );
		?>
	</ol>
<?php endif;

edent_internal_links_walker( get_the_permalink() );


if ( comments_open() || pings_open() ) :
	$comments_args = array(
		// Change the title of the reply section
		'title_reply' => __( 'What are your reckons?', 'textdomain' ),
		//	Change the note before the form
		"comment_notes_before" => "<p>All comments are moderated and may not be published immediately. Your email address will <em>not</em> be published.</p>",
		// Text or HTML to be displayed after the set of comment fields
		'comment_notes_after' => "<p>Allowed HTML: <code>" . allowed_tags() . "</code></p>",
		//	Force client side validation 
		"format" => "xhtml",
		"comment_field" => '<label for="comment">Comment:</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" required placeholder="Write something interesting here…"></textarea>',
		'fields' => array(
			'author' => '<label for="author">Your Name (required):</label> <input id="author" name="author" type="text" value="" size="30" maxlength="245" autocomplete="name" placeholder="Dr. Winston O\'Boogie" required>',
			'email' => '<label for="email">Your Email (required):</label> <input id="email" name="email" type="email" placeholder="me@example.com" required>',
			'url' => '<label for="url">Your Website (optional):</label> <input id="url" name="url" type="url" pattern="^https?:\/\/(.*)" placeholder="https://example.com">'
		)
	);
	comment_form( $comments_args, $postID );
else : ?>
	<p class="closed"><?php _e( 'Comments closed', 'edent' ); ?></p>

<?php endif; ?>
</section>
