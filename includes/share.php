<?php
//	Better Sharing Buttons
function edent_sharing_buttons( $link, $title ) {
	$link  = urlencode( $link );
	$title = rawurlencode( html_entity_decode( $title ) );
	$assets = get_template_directory() . "/assets/images/";
	$mastodon   = inline_svg( $assets . "/mastodon.svg"   );
	$twitter    = inline_svg( $assets . "/twitter.svg"    );
	$facebook   = inline_svg( $assets . "/facebook.svg"   );
	$linkedin   = inline_svg( $assets . "/linkedin.svg"   );
	$bsky       = inline_svg( $assets . "/bsky.svg"       );
	$threads    = inline_svg( $assets . "/threads.svg"       );
	$reddit     = inline_svg( $assets . "/reddit.svg"     );
	$hackernews = inline_svg( $assets . "/hackernews.svg" );
	$lobsters   = inline_svg( $assets . "/lobsters.svg"   );
	$pocket     = inline_svg( $assets . "/pocket.svg"     );
	$whatsapp   = inline_svg( $assets . "/whatsapp.svg"   );
	$telegram   = inline_svg( $assets . "/telegram.svg"   );
	echo <<<EOT
	<hr class="hr-share">
	<section class="edent-share">
		<h2 id="share-the-love">Share this post on…</h2>
		<ul class="edent-share-list">
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://tootpick.org/#text=$title%20$link">
					<img class="edent-share-icon" alt="Mastodon" src="$mastodon" width="20"></a>
			</li>
	<!--
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://twitter.com/intent/tweet?via=edent&text=$title&url=$link">
					<img class="edent-share-icon" alt="Twitter" src="$twitter" width="20"></a>
			</li>
	-->	
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://www.facebook.com/sharer.php?u=$link&t=$title">
					<img class="edent-share-icon" alt="Facebook" src="$facebook" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://www.linkedin.com/sharing/share-offsite/?url=$link">
					<img class="edent-share-icon" alt="LinkedIn" src="$linkedin" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://bsky.app/intent/compose?text=$title%20$link">
					<img class="edent-share-icon" alt="BlueSky" src="$bsky" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://www.threads.net/intent/post?url=$link&text=$title">
					<img class="edent-share-icon" alt="Threads" src="$threads" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://www.reddit.com/submit?url=$link&title=$title">
					<img class="edent-share-icon" alt="Reddit" src="$reddit" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://news.ycombinator.com/submitlink?u=$link&t=$title">
					<img class="edent-share-icon" alt="HackerNews" src="$hackernews" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://lobste.rs/stories/new?url=$link&title=$title">
					<img class="edent-share-icon" alt="Lobsters" src="$lobsters" width="20"></a>
			</li>
	<!--
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://getpocket.com/save/?url=$link&title=$title">
					<img class="edent-share-icon" alt="Pocket" src="$pocket" width="20"></a>
			</li>
	-->
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://api.whatsapp.com/send/?text=$link">
					<img class="edent-share-icon" alt="WhatsApp" src="$whatsapp" width="20"></a>
			</li>
			<li class="edent-share-item">
				<a class="edent-share-link" rel="nofollow noopener noreferrer" target="_blank"
					href="https://telegram.me/share/url?url=$title&text=$link">
					<img class="edent-share-icon" alt="Telegram" src="$telegram" width="20"></a>
			</li>
		</ul>
	</section>
	EOT;
}