=== Edent ===
Contributors: edent
Donate link: https://www.paypal.me/edent/gbp5
Requires at least: 6.0
Tested up to: 6.4.3
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Installation ==

1. Upload the theme
2. Activate the theme

== Includes ==

* Mono Font - https://commitmono.com/
* Body Font - https://github.com/tryoxiss/atkinson-hyperlegible-pro/ processed through https://www.fontsquirrel.com/tools/webfont-generator

== Changelog ==

See https://gitlab.com/edent/blog-theme/-/commits/master/?ref_type=HEADS

Version 1.5 (2018-12-09)
------------------------
- Forked from https://en-gb.wordpress.org/themes/davis/