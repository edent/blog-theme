<?php 
function edent_relationship_graph( $postID ) {

	$display = false;

	//	Function to add new nodes
	function add_item_to_nodes( &$nodes, $id, $label, $url, $group ) {
		$nodes[] = [ 
			"id"    => $id, 
			"label" => $label, 
			"url"   => $url, 
			"group" => $group
		];
	}

	//	Function to add new relationships
	function add_relationship( &$links, $source, $target ) {

		//	Ignore self-links
		if ( $source == $target ) {
			return;
		}

		$links[] = [
			"source" => $source,
			"target" => $target
		];
	}

	//	Set up arrays for nodes and links
	$nodes = array();
	$links = array();

	//	ID of the Post
	$main_post_id = $postID;

	//	Get the Post's details
	$main_post_url   = get_permalink( $main_post_id );
	$main_post_title = get_the_title( $main_post_id );

	//	Add Post to the nodes
	add_item_to_nodes( $nodes, $main_post_id, $main_post_title, $main_post_url, "main" );

	//	Get the tags of the Post
	$main_post_tags = get_the_tags( $main_post_id );

	//	Add the tags as nodes, and create links to main Post
	if ( false != $main_post_tags ) {
		foreach( $main_post_tags as $tag ) {
			$id   = $tag->term_id;
			$name = $tag->name;
	
			//	Add the node
			add_item_to_nodes( $nodes, $id, $name, get_tag_link( $id ), "tag" );
			//	Add the relationship
			add_relationship( $links, $id, $main_post_id );
		}	
	}

	//	Get all the posts which link to this one, oldest first
	$the_query = new WP_Query(
		array(
			's'              => $main_post_url,
			'post_type'      => 'post',
			"posts_per_page" => "-1",
			"order"          => "ASC"
		)
	);

	//	Nothing to do if there are no inbound links
	if ( $the_query->have_posts() ) {
		//	Loop through the posts
		while ( $the_query->have_posts() ) {
			//	Set up the query
			$the_query->the_post();
			$post_id = get_the_ID();
			$title = esc_html( get_the_title() );
			$url   = get_the_permalink();

			//	Ignore self-links
			if ( $post_id === $main_post_id ) {
				$display = false;
				continue;
			}
			$display = true;

			//	Add the node
			add_item_to_nodes( $nodes, $post_id, $title, $url, "post" );
			//	Add the relationship
			add_relationship( $links, $post_id, $main_post_id );

			//	Get the tags of the Post
			$post_tags = get_the_tags( $post_id );

			//	Add the tags as nodes, and create links to main Post
			if ( false != $post_tags ){
				foreach($post_tags as $tag) {

					$id   = $tag->term_id;
					$name = $tag->name;
				
					//	Add the node
					add_item_to_nodes( $nodes, $id, $name, get_tag_link( $id ), "tag" );
					//	Add the relationship
					add_relationship( $links, $id, $post_id );
				}
	
			}
		}
	}

	//	Get all the internal links from this post
	//	Render the post as HTML
	$content = apply_filters( "the_content", get_the_content( null, false, $main_post_id ) );

	$regex = <<< EOR
/https\:\/\/shkspr\.mobi\/blog\/\d{4}\/[^"']*/i
EOR;
	preg_match_all($regex, $content, $internal_links);

	//	Loop through the internal links, get their hashtags
	foreach ( $internal_links[0] as $url ) {

		//	Strip query strings and anchors
		$parsed_url = parse_url($url);
		$url = $parsed_url["scheme"] . "://" . $parsed_url["host"] . $parsed_url["path"];

		//	Get the ID
		$post_id = url_to_postid( $url );
		if ( $post_id == 0 ) {
			continue;
		}
		
		//	Ignore self-links
		if ( $post_id === $main_post_id ) {
			continue;
		}
		$display = true;

		//	Get the Post's details
		$post_title = get_the_title( $post_id );

		//	Add the node
		add_item_to_nodes( $nodes, $post_id, $post_title, $url, "post" );
		//	Add the relationship
		add_relationship( $links, $main_post_id, $post_id );

		//	Get the tags of the Post
		$post_tags = get_the_tags( $post_id );

		//	Add the tags as nodes, and create links to main Post
		if ( false != $post_tags ) {
			foreach( $post_tags as $tag ) {
				$id   = $tag->term_id;
				$name = $tag->name;
			
				//	Add the node
				add_item_to_nodes( $nodes, $id, $name, get_tag_link( $id ), "tag" );
				//	Add the relationship
				add_relationship( $links, $id, $post_id );
			}
		}
	}

	//	No point displaying if there are no inbound or outbound links
	if ( !$display ) {
		return "";
		die();
	}

	//	Deduplicate the nodes and links
	$nodes_unique = array_unique( $nodes, SORT_REGULAR );
	$links_unique = array_unique( $links, SORT_REGULAR );

	//	Put them in the keyless format that D3 expects
	$nodes_output = array();
	$links_output = array();

	foreach ( $nodes_unique as $node ) {
		$nodes_output[] = $node;
	}

	foreach ( $links_unique as $link ) {
		$links_output[] = $link;
	}

	$theme = get_theme_file_uri();
	$js = $theme . "/assets/js/d3.v7.min.js";

	//	Show the JS + SVG
	$output = <<< EOT
	<aside><section>
	<h2 id="fd-graph">Interactive Relationship Graph</h2>
	<script src="{$js}"></script>

	<svg id="fd-graph-relationships">
		<defs>
			<marker id="arrowhead" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto" fill="#999">
			<path d="M0,0 L10,3.5 L0,7 Z"></path>
			</marker>
		</defs>
	</svg>

	<script>
		const nodes =
	EOT;

	//	Return the JSON
	$output .= json_encode( $nodes_output, JSON_PRETTY_PRINT ) . ";\n";
	$output .= "const links = ";
	$output .= json_encode( $links_output, JSON_PRETTY_PRINT ) . ";\n";

	//	Rest of JS + SVG
	$output .= <<< 'EOT'
		//	If this is a narrow viewport, make image smaller
		const width  = Math.min(1024, document.documentElement.clientWidth);
		const height = 400;
		const strength = -100;
		const bgColour = "var(--color-box)";
		const textColour = "var(--color-text)";

		const svg = d3.select("#fd-graph-relationships")
			.attr( "width",  width  )
			.attr( "height", height )
			.style( "max-width", "100%" )
			.style( "background", bgColour );


		const simulation = d3.forceSimulation( nodes )
			.force( "link",   d3.forceLink( links ).id( d => d.id ).distance( 100 ) )
			.force( "charge", d3.forceManyBody().strength( strength ) )
			.force( "center", d3.forceCenter( width / 2, height / 2 ) );

		//	Run simulation with simple animation
		simulation.on("tick", () => {
			link
				.attr( "x1", d => d.source.x )
				.attr( "y1", d => d.source.y )
				.attr( "x2", d => d.target.x )
				.attr( "y2", d => d.target.y );
			node
				.attr( "transform", d => `translate(${d.x},${d.y})` );
		});

		// Draw links
		const link = svg.selectAll( ".link" )
			.data( links )
			.enter().append( "line" )
			.attr( "stroke", "#999" )
			.attr( "stroke-width", 2 )
			.attr( "x1", d => d.source.x )
			.attr( "y1", d => d.source.y )
			.attr( "x2", d => d.target.x )
			.attr( "y2", d => d.target.y )
			.attr( "marker-end", "url(#arrowhead)" );

		//	Draw nodes
		const node = svg.selectAll( ".node" )
			.data( nodes )
			.enter().append( "g" )
			.attr( "class", "node" )
			.attr( "transform", d => `translate(${d.x},${d.y})` )
			.call(d3.drag()	//	Make nodes draggable
				.on( "start", dragStarted )
				.on( "drag",  dragged )
				.on( "end",   dragEnded ) 
			);
		
		//	Add hyperlink
		node.append("a")
		.attr( "xlink:href", d => d.url ) //	Link to the node's URL
		.attr( "target", "_blank" ) //	Open in a new tab
		.each(function ( d ) {
			const a = d3.select( this );
			//	Different shapes for posts and tags
			if ( d.group === "post" || d.group === "main" ) {
				a.append( "circle" )
					.attr( "r", 10 )
					.attr( "fill",  d => ( d.group === "main" ? "blue" : "#bfd0ff" ) );
			} else if ( d.group === "tag" ) {
				//	White background rectangle
				a.append("rect")
						.attr( "width",  20 )
						.attr( "height", 20 )
						.attr( "x", -10 )
						.attr( "y", -10 )
						.attr( "fill", bgColour ); 
				// Red octothorpe
				a.append("path")
						.attr( "d", "M-10,-5 H10 M-10,5 H10 M-5,-10 V10 M5,-10 V10" ) 
						.attr( "stroke", "red" )
						.attr( "stroke-width", 2 )
						.attr( "fill", "none" );
			}
			//	Text label
			// a.append( "text")
			// 	.attr( "dy", 4 )
			// 	.attr( "x", d => ( d.group === "post" ? 12 : 14 ) )
			// 	.attr( "fill", textColour )
			// 	.style( "font-size", "1em" )
			// 	.style( "font-family", "sans" )
			// 	.style( "font-weight", d => ( d.group === "main" ? "bold"   : "normal" ) )
			// 	.style( "font-style",  d => ( d.group === "tag"  ? "italic" : "normal" ) )
			// 	.text( d.label );
			a.append( "title" ).text( d.label );
		});

		//	Standard helper functions to make nodes draggable
		function dragStarted( event, d ) {
			if ( !event.active ) simulation.alphaTarget(0.3).restart();
			d.fx = d.x;
			d.fy = d.y;
		}
		function dragged( event, d ) {
			d.fx = event.x;
			d.fy = event.y;
		}
		function dragEnded( event, d ) {
			if (!event.active) simulation.alphaTarget(0);
			d.fx = null;
			d.fy = null;
		}
	</script>
</section></aside>
EOT;

	//	Go back to the original loop
	wp_reset_query();
	return $output;
}
